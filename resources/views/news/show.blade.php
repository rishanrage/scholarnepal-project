@extends('layouts.main')

@section('title')
 {{ $news->title }} | Scholar Nepal :: Digital Journal
@endsection


@section('content')

  <div class="container">
    <div class="row">
       <div class="col-md-10" style="padding-left: 5%">

    <h1 class="my-5" style="border-bottom: 1px solid; line-height: 54px;
                            padding-bottom: 15px;">{{ $news->title }}
                            </h1>

  @if(!empty($news->description))
    <div class="card">
      <div class="shadow-sm">
        {!! $news->description !!}
      </div>  
    </div> 
  @endif         

   @if(!empty($news->image))
      <img src="{{ asset('data/'.$news->image ) }}" /> 
    @endif                       

<div style="height: 25px"></div> 
     {!! $news->content !!}

     </div>
 <div class="col-md-10" style="padding-left: 5%">
 <h3 class="my-5" style="line-height: 30px;
                            padding-bottom: 0px;">Releated
                            </h3>

    <ul class="Releated">


    @foreach($releated as $key=> $r)

        <li>
        <a href="{{ route('news.show', $r->id) }}">
           @if(!empty($news->image))
            <img src="{{ asset('data/'.$r->image ) }}" /> 
          @endif  
          {{ $r->title }}
        </a></li>

    @endforeach  
     
  
     
    </ul>                        

<div style="height: 45px; width: 100%; overflow: hidden;"></div>
 </div>

   </div>
</div>

<style type="text/css">
	.shadow-sm {
    box-shadow: 0 .125rem .25rem rgba(0,0,0,.075) !important;
    box-shadow: 0 .125rem .25rem rgba(0,0,0,.075) !important;
	padding: 25px;
	margin: 65px;
	font-size: 18px;
	line-height: 30px;
	background: #f1f1f1;
}
p{
	font-size: 17px;
	line-height: 29px;
}

.Releated li{
  width: 30%; float: left; margin-right: 25px;
}
.Releated img {
  margin-bottom: 10px; border-radius: 4px;
}
</style>

@endsection