@extends('layouts.app')

@section('title')
  Scholar Nepal | Users
@endsection

@section('content')

<!-- <div class="d-flex justify-content-end mb-2">
 <a href="{{ route('tags.create') }}" class="btn btn-success">Add tag</a>
</div> -->

<div class="card card-default">
  <div class="card-header">
     Users
  </div>

  <div class="card-body">
   @if($users->count() > 0)
     <table class="table">
      <thead> 
        <th>Image</th>
        <th>Name</th>
        <th>Email</th>
        <th>Action</th>
      </thead>
      <tbody>
      @foreach($users as $user)
        <tr>
         <td>
           <img style="width: 40px; height: 40px; border-radius: 50%" src="{{ Gravatar::src($user->email) }}" alt=""/>
         </td>
          <td>{{ $user->fname }} {{ $user->lname }}</td>
          <td>{{ $user->email }}</td>
          <td>
          @if(!$user->isAdmin())
           <form action="{{ route('users.make-admin', $user->id) }}" method="POST" class="float-left mx-1">
           @csrf
            <button type="submit" class="btn btn-success">Make Admin</button>
           </form>
          @endif
          	<a href="#" class="btn btn-info">Active</a>
            <button class="btn btn-info btn-danger" onclick="handleDelete({{ $user->id }})">Delete</button>
          </td>
        </tr>
      @endforeach
      </tbody>   	
     </table> 
    @else
     <h6 class="text-center">No user yet</h6>
    @endif  


    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog">

        <form action="" method="POST" id="deleteuser">
           @method('DELETE')
           @csrf          
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Delete user</h5>
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">
                <p class="text-center">Are you sure you want to delete this user?</p>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No, Go back</button>
                <button type="submit" class="btn btn-danger">Yes, Delete</button>
              </div>
            </div>
          </form>

      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
  
  function handleDelete(id) {    
    var form = document.getElementById('deleteuser')
    form.action = '/users/' + id
    console.log('deleting.', form);
    $('#deleteModal').modal('show')
  }

</script>

@endsection