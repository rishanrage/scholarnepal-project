@extends('layouts.app')

@section('content')
    <div class="card">

        <div class="card-header">Profile</div>

        <div class="card-body">

           <form action="{{ route('users.update-profile') }}" method="POST">
             @csrf
             @method('PUT')

          @if($user->type === 2)    

             <div class="form-group">
               <label for="name_institution">Name of Institution</label>
               <input type="text" id="name_institution" name="name_institution" class="form-control" value="{{ $user->name_institution }}">
             </div> 
             <div class="form-group">
               <label for="location">Location</label>
               <input type="text" id="i_location" name="location" class="form-control" value="{{ $user->location }}">
             </div>
             <div class="form-group">
               <label for="email">Email</label>
               <input type="email" id="i_email" name="email" class="form-control" value="{{ $user->email }}" readonly>
             </div>
             <div class="form-group">
               <label for="c_person">Contact Person</label>
               <input type="text" id="i_c_person" name="c_person" class="form-control" value="{{ $user->fname }} {{ $user->lname }}">
             </div>
             <div class="form-group">
               <label for="pnone">Phone</label>
               <input type="text" id="i_pnone" name="pnone" class="form-control" value="{{ $user->pnone }}">
             </div>
             <div class="form-group">
               <label for="r_areas">Research Areas</label>
               <input type="text" id="i_r_areas" name="r_areas" class="form-control" value="{{ $user->r_areas }}">
             </div>

        @elseif($user->type === 1)

             <div class="form-group">
               <label for="fname">First Name</label>
               <input type="text" id="fname" name="fname" class="form-control" value="{{ $user->fname }}">
             </div>
             <div class="form-group">
               <label for="lname">Last Name</label>
               <input type="text" id="lname" name="lname" class="form-control" value="{{ $user->lname }}">
             </div>
             <div class="form-group">
               <label for="email">Email</label>
               <input type="text" id="email" name="email" class="form-control" value="{{ $user->email }}" readonly>
             </div>
             <div class="form-group">
               <label for="location">Location</label>
               <input type="text" id="location" name="location" class="form-control" value="{{ $user->location }}">
             </div>
              <div class="form-group">
               <label for="location">Gender</label>
               <select name="gender" class="form-control">
               <option>Select Gender</option>
                   <option value="male">Male</option>
                   <option value="female">Female</option>
                   <option value="other">Other</option>
               </select>
             </div>
             <div class="form-group">
               <label for="dob">DOB</label>
               <input type="text" id="dob" name="dob" class="form-control" value="{{ $user->dob }}">
             </div>
             <div class="form-group">
               <label for="c_profession">Current Profession</label>
               <input type="text" id="c_profession" name="c_profession" class="form-control" value="{{ $user->c_profession }}">
             </div>
              <div class="form-group">
               <label for="work_place">Current Place of Profession</label>
               <input type="text" id="work_place" name="work_place" class="form-control" value="{{ $user->work_place }}">
             </div>
             <div class="form-group">
               <label for="a_degree">Academic Degree</label>
               <input type="text" id="a_degree" name="a_degree" class="form-control" value="{{ $user->a_degree }}">
             </div>
             <div class="form-group">
               <label for="a_institution">Academic Institution</label>
               <input type="text" id="a_institution" name="a_institution" class="form-control" value="{{ $user->a_institution }}">
             </div>
             <div class="form-group">
               <label for="r_areas">Research Areas</label>
               <input type="text" id="r_areas" name="r_areas" class="form-control" value="{{ $user->r_areas }}">
             </div>

        

        @elseif($user->type === 3)    

             <div class="form-group">
               <label for="name_institution">Name of Organization</label>
               <input type="text" id="org_institution" name="name_institution" class="form-control" value="{{ $user->name_institution }}">
             </div> 
             <div class="form-group">
               <label for="t_organisation">Type of Organization</label>
               <input type="text" id="t_organisation" name="t_organisation" class="form-control" value="{{ $user->t_organisation }}">
             </div> 
             <div class="form-group">
               <label for="location">Location</label>
               <input type="text" id="org_location" name="location" class="form-control" value="{{ $user->location }}">
             </div>
             <div class="form-group">
               <label for="email">Email</label>
               <input type="email" id="org_email" name="email" class="form-control" value="{{ $user->email }}" readonly >
             </div>
             <div class="form-group">
               <label for="c_person">Contact Person</label>
               <input type="text" id="org_c_person" name="c_person" class="form-control" value="{{ $user->c_person }}">
             </div>
             <div class="form-group">
               <label for="phone">Phone</label>
               <input type="text" id="org_phone" name="phone" class="form-control" value="{{ $user->phone }}">
             </div>
             <div class="form-group">
               <label for="r_areas">Research Areas</label>
               <input type="text" id="org_r_areas" name="r_areas" class="form-control" value="{{ $user->r_areas }}">
             </div>     

        @endif
        
             <button type="submit" class="btn btn-success">Update Profile</button>
               
           </form>

        </div>

    </div>
@endsection
