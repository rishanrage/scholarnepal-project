@extends('layouts.app')

@section('title')
  Scholar Nepal | Edit Profile
@endsection

<!-- Individual User
Academic Institute

 -->
@section('content')


 <div class="card card-default">

        <div class="card-header">
        <span style="text-align: left;">Profile </span>
        <span style="color: #666; font-size: 12px;">
           @if($user->type == 1)
            Individual User
           @elseif($user->type == 2)
            Academic Institute
           @elseif($user->type == 3) 
            Organisation 
           @endif
        </span>
        </div>

        <div class="card-body">

           <form action="{{ route('users.update-profile') }}" method="POST" enctype="multipart/form-data">
             @csrf
             @method('PUT')

        @if($user->type === 1)


 <div class="form-group row">

             <div class="col-md-6">
               <label for="fname">First Name</label>
               <input type="text" id="fname" name="fname" class="form-control" value="{{ $user->fname }}" placeholder="First Name">
             </div>

             <div class="col-md-6">
               <label for="lname">Last Name</label>
               <input type="text" id="lname" name="lname" class="form-control" value="{{ $user->lname }}" placeholder="Last Name">
             </div>
</div>             

<div class="form-group row">

             <div class="col-md-6">
               <label for="email">Email</label>
               <input type="text" id="email" name="email" class="form-control" value="{{ $user->email }}" readonly placeholder="Email">
             </div> 

             <div class="col-md-6">
               <label for="location">Location</label>
               <input type="text" id="location" name="location" class="form-control" value="{{ $user->location }}" placeholder="Location">
             </div>
</div>     

<div class="form-group row">

              <div class="col-md-6">
               <label for="location">Gender</label>
               <!-- {{ $user->gender }} -->
               <select name="gender" class="form-control">
               <option>Select Gender</option>

                   <option value="male" {{ $user->gender == 'male' ? 'selected' : ''}}>
                      Male
                   </option>
                   <option value="female" {{ $user->gender == 'female' ? 'selected' : ''}}>
                     Female
                    </option>
                   <option value="other" {{ $user->gender == 'other' ? 'selected' : ''}}>
                     Other
                   </option>
               </select>
             </div>

             <div class="col-md-6">
               <label for="dob">DOB</label>
               <input type="text" id="dob" name="dob" class="form-control" value="{{ $user->dob }}" placeholder="DOB">
             </div>

</div>             

<div class="form-group row">

             <div class="col-md-6">
               <label for="c_profession">Current Profession</label>
               <input type="text" id="c_profession" name="c_profession" class="form-control" value="{{ $user->c_profession }}" placeholder="Current Profession">
             </div>

              <div class="col-md-6">
               <label for="work_place">Current Place of Profession</label>
               <input type="text" id="work_place" name="work_place" class="form-control" value="{{ $user->work_place }}" placeholder="Current Place of Profession">
             </div>

</div>             

<div class="form-group row">

             <div class="col-md-6">
               <label for="a_degree">Academic Degree</label>
               <input type="text" id="a_degree" name="a_degree" class="form-control" value="{{ $user->a_degree }}" placeholder="Academic Degree">
             </div>

             <div class="col-md-6">
               <label for="a_institution">Academic Institution</label>
               <input type="text" id="a_institution" name="a_institution" class="form-control" value="{{ $user->a_institution }}" placeholder="Academic Institution">
             </div>

</div> 

<div class="form-group row">

             <div class="col-md-6">
               <label for="r_areas">Research Areas</label>
               <input type="text" id="r_areas" name="r_areas" class="form-control" value="{{ $user->r_areas }}" placeholder="Research Areas">
             </div>

  </div>  

  @if(isset($user))
    @if(!empty($user->image))
       <img src="{{ asset('data/'.$user->image) }}" width="60" />    
     @endif
  @endif

  <div class="form-group row">
             <div class="col-md-6">
               <label for="image">Photo</label>
       
               <input type="file" id="image" name="image" class="form-control"  value="{{ asset('data/'.$user->image) }}">             
             </div>     
  </div>       

        @elseif($user->type === 2)   

            <input type="hidden" name="fname" value="{{ $user->fname }}">
            <input type="hidden"  name="lname"  value="{{ $user->lname }}"> 

<div class="form-group row">

             <div class="col-md-6">
               <label for="name_institution">Name of Institution</label>
               <input type="text" id="name_institution" name="name_institution" class="form-control" value="{{ $user->name_institution }}">
             </div> 

             <div class="col-md-6">
               <label for="location">Location</label>
               <input type="text" id="location" name="location" class="form-control" value="{{ $user->location }}">
             </div>

</div>

<div class="form-group row">

             <div class="col-md-6">
               <label for="email">Email</label>
               <input type="email" id="email" name="email" class="form-control" value="{{ $user->email }}" readonly>
             </div>

             <div class="col-md-6">
               <label for="c_person">Contact Person</label>
               <input type="c_person" id="c_person" name="c_person" class="form-control" value="{{ $user->fname }} {{ $user->lname }}">
             </div>

</div>

<div class="form-group row">

             <div class="col-md-6">
               <label for="phone">Phone</label>
               <input type="text" id="phone" name="phone" class="form-control" value="{{ $user->phone }}">
             </div>

             <div class="col-md-6">
               <label for="r_areas">Research Areas</label>
               <input type="text" id="r_areas" name="r_areas" class="form-control" value="{{ $user->r_areas }}">
             </div>

</div>

@if(isset($user))
    @if(!empty($user->image))
       <img src="{{ asset('data/'.$user->image) }}" width="60" />    
     @endif
  @endif

  <div class="form-group row">
             <div class="col-md-6">
               <label for="image">Photo</label>
       
               <input type="file" id="image" name="image" class="form-control"  value="{{ asset('data/'.$user->image) }}">             
             </div>     
  </div>

        @elseif($user->type === 3)  

            <input type="hidden" name="fname" value="{{ $user->fname }}">
            <input type="hidden"  name="lname"  value="{{ $user->lname }}"> 


<div class="form-group row">

             <div class="col-md-6">
               <label for="name_institution">Name of Organization</label>
               <input type="text" id="name_institution" name="name_institution" class="form-control" value="{{ $user->name_institution }}">
             </div> 

             <div class="col-md-6">
               <label for="t_organisation">Type of Organization</label>
               <input type="text" id="t_organisation" name="t_organisation" class="form-control" value="{{ $user->t_organisation }}">
             </div> 

 </div> 

<div class="form-group row">

             <div class="col-md-6">
               <label for="location">Location</label>
               <input type="text" id="location" name="location" class="form-control" value="{{ $user->location }}">
             </div>

             <div class="col-md-6">
               <label for="email">Email</label>
               <input type="email" id="email" name="email" class="form-control" value="{{ $user->email }}" readonly >
             </div>

</div>            


<div class="form-group row">

             <div class="col-md-6">
               <label for="c_person">Contact Person</label>
               <input type="text" id="c_person" name="c_person" class="form-control" value="{{ $user->c_person }}">
             </div>

             <div class="col-md-6">
               <label for="phone">Phone</label>
               <input type="text" id="phone" name="phone" class="form-control" value="{{ $user->phone }}">
             </div>

 </div>

  <div class="form-group row">
             <div class="col-md-6">
               <label for="r_areas">Research Areas</label>
               <input type="text" id="r_areas" name="r_areas" class="form-control" value="{{ $user->r_areas }}">
             </div>     
</div>


  @if(isset($user))
    @if(!empty($user->image))
       <img src="{{ asset('data/'.$user->image) }}" width="60" />    
     @endif
  @endif

  <div class="form-group row">
             <div class="col-md-6">
               <label for="image">Photo</label>
       
               <input type="file" id="image" name="image" class="form-control"  value="{{ asset('data/'.$user->image) }}">             
             </div>     
  </div>
        @endif
             <button type="submit" class="btn btn-success">Update Profile</button>
               
           </form>

        </div>

    </div>
@endsection


@section('scripts')

<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

<script type="text/javascript">
  flatpickr('#dob')
</script>
@endsection

@section('css')
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css" />
 <style type="text/css">
   select {
    height: 50px !important;
    padding: 5px 15px !important;
}
 </style>
@endsection