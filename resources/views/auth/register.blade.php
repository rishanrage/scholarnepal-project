@extends('layouts.main')
@section('title')
  Registration | Scholar Nepal :: Digital Journal
@endsection

@section('content')
<div class="container" style="padding: 35px 15px;">
    <div class="row justify-content-center">
    <div class="col-md-3 col-md-offset-1" style="margin-top: 17px;">
    <img src="{{ asset('theme/images/13fabe368d08211706da14d461727b50.gif') }}" 
         style="width: 100%; height: 56%" />
    </div>
        <div class="col-md-7">
          <!-- <div class="col-md-7 col-md-offset-3"> -->
            <div class="card">
                <!-- <div class="card-header">{{ __('Register') }}</div> -->
                <h4><i class="fas fa-edit"></i> Join Scholar Nepal</h4>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">


                     <!--        <label for="fname" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label> -->

                            <div class="col-md-6">
                                <input id="fname" type="text" class="form-control{{ $errors->has('fname') ? ' is-invalid' : '' }}" name="fname" value="{{ old('fname') }}" required autofocus placeholder="First Name">

                                @if ($errors->has('fname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                @endif
                            </div>

<!-- 
                        </div>

                        <div class="form-group row">
 -->

                      <!--       <label for="lname" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label> -->

                            <div class="col-md-6">
                                <input id="lname" type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}" name="lname" value="{{ old('lname') }}" required autofocus placeholder="Last Name">

                                @if ($errors->has('lname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                                @endif
                            </div>


                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                           <div class="col-md-12">
                    <!--         <div class="col-md-6">

                                  <select id="type"  class="form-control" name="type" required>
                               

                                    <option value="1">Individual User</option>
                                    <option value="2">Academic Institute</option>
                                    <option value="3">Organisation</option>
                                  </select>


                                @if ($errors->has('type'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div> -->
                            <div class="col-md-4">
                            <input type="radio" id="1" name="type" value="1">
                            Individual User
                            </div>
                             <div class="col-md-4">
                             <input type="radio" id="2" name="type" value="2">
                             Academic Institute
                            </div>
                             <div class="col-md-4">
                             <input type="radio" id="3" name="type" value="3">
                             Organisation
                            </div>
                            </div>
                        </div>

                        <div class="form-group row">


                <!--             <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
 -->

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

<!-- 
                        </div>

                        <div class="form-group row">


                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label> -->

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                            </div>


                        </div>

                        <div class="form-group row">
                           <div class="col-md-12">
                              <input type="checkbox" name="agree" required="" style="float: left; min-height: 23px; margin: 0px; padding: 10px;">
                              <p style="margin: 0px 0 0 30px;"> I agree to privacy policy, disclaimer, and terms and condition. </p>
                           </div>
                         </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="buttton_register">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>

                        <hr style="border-top:1px solid #000 !important; padding: 20px 0 10px 0;">
                    </form>

<p class="link-bottom">Are you a member ?  <a href="{{ route('login') }}"> Login now</a></p>

                </div>
            </div>
        </div>




    </div>
</div>
@endsection

<style type="text/css">
input[type=checkbox] {
    transform: scale(1.5);
    float: left; min-height: 23px; margin-right: 15px !important;
}

input[type=radio] {
    transform: scale(1.5);
    float: left; min-height: 22px; margin-right: 15px !important;
}
    .nice-select {
        line-height: 35px !important;
        margin-bottom: 0px !important;
    }
    .form-group {
        margin-bottom: 25px !important;
    }
   .buttton_register {
    background: #ffb606 none repeat scroll 0 0;
    border: medium none;
    display: inline-block;
    font-family: "Poppins",sans-serif;
    font-weight: 600;
    margin-top: 10px;
    padding: 8px 50px;
    text-transform: uppercase;
    color: #232323;
    }
    .fa-edit {
        border: 1px solid #666;
        padding: 10px 13px;
        border-radius: 50%;
        margin-right: 10px;

    }

</style>