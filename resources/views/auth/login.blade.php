@extends('layouts.main')

@section('title')
  Login | Scholar Nepal :: Digital Journal
@endsection

@section('content')


<div class="container" style="padding: 35px 0">
            <div class="row">
             <div class="col-md-3 col-md-offset-2" style="margin-top: 17px;">
                <img src="{{ asset('theme/images/13fabe368d08211706da14d461727b50.gif') }}" 
                     style="width: 100%; height: 50%" />
                </div>
                <div class="col-md-5">
                      <form method="POST" action="{{ route('login') }}" class="white-popup-block">
                        @csrf
                        <div class="login-custom">
                            <div class="heading">
                                <h4><i class="fas fa-sign-in-alt"></i> Login Now</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <label for="login-remember">

                  <input  type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                  Remember Me</label>

                                @if (Route::has('password.request'))
                                    <a title="Lost Password" class="lost-pass-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">

                               <button type="submit">
                                    {{ __('Login') }}
                                </button>



                                </div>
                            </div>
                            <p class="link-bottom">Not a member yet ?  <a href="{{ route('register') }}">  Join our community</a></p>
                        </div>
                        <!-- <div class="login-social">
                            <h4>Login with social</h4>
                            <ul>
                                <li class="facebook">
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li class="twitter">
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="linkedin">
                                    <a href="#">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                            </ul>
                        </div> -->
                    </form>
                </div>
            </div>
        </div>





<div class="container" style="display: none;">
    <div class="row">
        <div class="col-md-7">
       
                <div class="card-header">{{ __('Login') }}</div>


                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
           
         
        </div>
    </div>
</div>
@endsection

<style type="text/css">
.form-group {
        margin-bottom: 25px !important;
    }
    .fa-sign-in-alt {
        border: 1px solid #666;
        padding: 10px 13px;
        border-radius: 50%;
        margin-right: 10px;

    }
    .nav.navbar.bootsnav {box-shadow:  none !important;}
    
</style>    