@extends('layouts.app')

@section('title')
  Scholar Nepal | Create Tags
@endsection

@section('content')

<div class="d-flex justify-content-end mb-2">
 <a href="{{ route('tags.create') }}" class="btn btn-success">Add tag</a>
</div>

<div class="card card-default">
  <div class="card-header">
     tags <span class="badge bg-warning">{{ $tags->count() }}</span> 
  </div>

  <div class="card-body">
   @if($tags->count() > 0)
     <table class="table">
      <thead> 
        <th>Name</th>
        <th>Posts Count</th>
        <th>Action</th>
      </thead>
      <tbody>
      @foreach($tags as $tag)
        <tr>
          <td>{{ $tag->name }}</td>
          <td>{{ $tag->posts->count() }} </td>
          <td>
          	<a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-info btn-info">Edit</a>
            <button class="btn btn-info btn-danger" onclick="handleDelete({{ $tag->id }})">Delete</button>
          </td>
        </tr>
      @endforeach
      </tbody>   	
     </table> 
    @else
     <h6 class="text-center">No tag yet</h6>
    @endif  


    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog">

        <form action="" method="POST" id="deletetag">
           @method('DELETE')
           @csrf          
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Delete tag</h5>
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">
                <p class="text-center">Are you sure you want to delete this tag?</p>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No, Go back</button>
                <button type="submit" class="btn btn-danger">Yes, Delete</button>
              </div>
            </div>
          </form>

      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
  
  function handleDelete(id) {    
    var form = document.getElementById('deletetag')
    form.action = '/tags/' + id
    console.log('deleting.', form);
    $('#deleteModal').modal('show')
  }

</script>

@endsection