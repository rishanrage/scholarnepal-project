@extends('layouts.main')
@section('title')
 Search | Scholar Nepal :: Digital Journal
@endsection

@section('content')

  <?php
    use Illuminate\Support\Str;
    Str::macro('readDuration', function(...$text) {
        $totalWords = str_word_count(implode(" ", $text));
        $minutesToRead = round($totalWords / 100);

        return (int)max(1, $minutesToRead);
    });
  ?>

<div class="row" style="
                   /*background: #fff7ee; */
                   /*text-align: center; */ padding-left: 13%;
                   margin-bottom: 40px;">
  <div class="col-md-10" style="border-bottom: 1px solid #666; padding-left: 0%;">
   <h1 style="color: #666;  font-size: 25px;
    /*margin-left: 13.333%; */
    padding: 25px 0 10px 0">Search Results for <span style="color: #14a5da;">{{request()->input('query')}}</span></h1>
    <p style="margin: -22px 0 0 0;">Results Available : {{ $result->count() }}</p>
  </div>
</div>

 <div class="container">

    <div class="row">

      <div class="col-md-10 col-md-offset-1">
          
			
		<div class="event-items">
               
             @if($result->count() > 0)   
             @foreach($result as $l)
                <div class="item">
                            <div class="col-md-4 thumb" style="padding-left: 0px;">
                            	<img src="{{ asset('data/'.$l->image) }}" height="150">
                            </div>
                            <div class="col-md-7 info">
                                <div class="info-box">
                                    <div class="date">
                                        <!-- <strong>16</strong> Apr, 2020 -->
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                {{ date('d-m-Y', strtotime($l->published_at)) }}

                                |  {{ \Carbon\Carbon::parse($l->published_at)->diffForHumans() }}

                                |  <i class="fas fa-clock"></i>
                                 <!-- 5 min to Read -->
                                  
                                  {{Str::readDuration($l->content). ' min to read'}}
                                   
                                    </div>
                                    <div class="content">
                                        <h4>
                                            <a href="{{ route('blog.show', $l->id) }}" style="line-height: 27px;">
                                               {{ $l->title }}
                                            </a>
                                        </h4>
                                      <!--   <ul>
                                            <li><i class="fas fa-clock"></i> 5 min to Read</li>
                                        </ul> -->
                                        <p style="font-size: 17px;">
                                
                                        {{ str_limit($l->description,160) }}

                                        </p>
      

                                    </div>
                                </div>
                            </div>
                </div>
                <div style="clear: both; height: 35px;"></div>
             @endforeach
             @else
                <p style="padding: 100px">No result found.</p>
             @endif
                        
         </div>

				

        </div>
    </div>
</div>

<style type="text/css">
	.fa-clock, .fa-calendar{
		color: #ffc41c;
	}
	.date{
		margin-bottom: 5px;
	}
	.content h4 {
		margin-bottom: 10px;
	}
	.thumb img {
		width: 100%;
	}


  .dropdown-item{
    display: block;
  }
  .dropdown-item:hover{
    color: #666;
  }
  /*.dropdown-menu{
    padding: 15px !important;
  }*/
/*  body{
    font-size: 16px !important;
  }*/
</style>
@endsection