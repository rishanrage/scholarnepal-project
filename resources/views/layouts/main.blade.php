<!DOCTYPE html>
<html lang="en">
<head>
<!-- ========== Meta Tags ========== -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Scholar Nepal :: Digital Journal">

<meta name="keywords" content="@yield('keywords')" />
<meta name="description" content="@yield('description')" />
<!-- ========== Page Title ========== -->
<title>
  @yield('title')
</title>

<!-- ========== Favicon Icon ========== -->
<link rel="shortcut icon" href="{{ asset('theme/images/ico.png') }}" type="image/x-icon">
<!-- ========== Start Stylesheet ========== -->
<link href="{{ asset('theme/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('theme/assets/css/font-awesome.min.css') }}" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
<link href="{{ asset('theme/assets/css/flaticon-set.css') }}" rel="stylesheet" />
<link href="{{ asset('theme/assets/css/themify-icons.css') }}" rel="stylesheet" />
<link href="{{ asset('theme/assets/css/magnific-popup.css') }}" rel="stylesheet" />
<link href="{{ asset('theme/assets/css/owl.carousel.min.css') }}" rel="stylesheet" />
<link href="{{ asset('theme/assets/css/owl.theme.default.min.css') }}" rel="stylesheet" />
<link href="{{ asset('theme/assets/css/animate.css') }}" rel="stylesheet" />
<link href="{{ asset('theme/assets/css/bootsnav.css') }}" rel="stylesheet" />
<link href="{{ asset('theme/style.css') }}" rel="stylesheet">
<link href="{{ asset('theme/assets/css/responsive.css') }}" rel="stylesheet" />
<link href="{{ asset('theme/assets/css/ticker.css') }}" rel="stylesheet" />

<!-- ========== Google Fonts ========== -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Girassol&display=swap" rel="stylesheet"> 
<style type="text/css">
    .dropdown-item:focus, .dropdown-item:hover {
    color: #16181b;
    text-decoration: none;
    background-color: #f8f9fa;
}
.dropdown-menu {
    padding:10px 0px !important;
}
.dropdown-menu .fa{
    color: #ffc41c;
    width:  20px; 
}
.dropdown-item {
    padding:0px 15px;
}
</style>

</head>
<body>

    <!-- Preloader Start -->
    <div class="se-pre-con"></div>
    <!-- Preloader Ends -->

    <!-- Header 
    ============================================= -->
    <header id="home">

        <!-- Start Navigation -->
        <nav class="navbar navbar-default logo-less small-pad navbar-sticky bootsnav">

            <!-- Start Top Search -->
            <div class="container">
                <div class="row">
                    <div class="top-search">
                        <div class="input-group">
                            <form action="#">
                                <input type="text" name="text" class="form-control" placeholder="Search">
                                <button type="submit">
                                    <i class="fas fa-search"></i>
                                </button>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container">

            <div class="attr-nav inc-btn">

            <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                          <li class="nav-item">
                                <a class="nav-link" href="#">
                                  Our Story
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                  Join us
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">
                                  Sign in
                                </a>
                            </li>
                       <!--       <li class="nav-item">
                                <a class="nav-link" href="#">
                                  <i class="ti-search"></i>
                                </a>
                            </li> -->
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">
                                    Get Started
                                    </a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a style="border:none; font-size: 15px; background: none;
                            font-weight: 400; text-transform: capitalize; color: #817A7A;" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre> 



                            @if(!empty(Auth::user()->image))

                                <img style=" margin-right: 10px; width: 30px; height: 30px; border-radius: 50%" src="{{asset('')}}data/{{ Auth::user()->image}}" 
                                    alt="{{ Auth::user()->fname }} {{ Auth::user()->lname }}"/>
                                @else
                                <img style=" margin-right: 10px; width: 30px; height: 30px; border-radius: 50%" src="{{ Gravatar::src(Auth::user()->lname) }}" alt="{{ Auth::user()->fname }} {{ Auth::user()->lname }}"/>
                                @endif

                                    {{ Auth::user()->fname }} {{ Auth::user()->lname }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                   
                                    
                                    <a class="dropdown-item" href="{{ route('home') }}">
                                    <i class="fa fa-pie-chart" aria-hidden="true"></i>
                                        Dashboard
                                    </a>
                                    <a class="dropdown-item" href="{{ route('users.edit-profile') }}">
                                       <i class="fa fa-user" aria-hidden="true"></i>
                                        Profile
                                    </a>
                                    
                                     <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-lock" aria-hidden="true"></i>
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                  <!--   <ul>
                        <li>
                            <a href="#">Sign Up</a>
                        </li>
                        <li>
                            <a href="#">Log In</a>
                        </li>
                    </ul> -->



                </div> 
                <!-- Start Atribute Navigation -->
              <div class="attr-nav inc-btn"  style="float: left;">
                   <a class="navbar-brand" href="/"  style="display: block;">
                        <img src="{{ asset('theme/images/scholarNepal_logo.png') }}" 
                        class="logo" alt="Logo" style="width: 80%">
                    </a>
                </div>       
                <!-- End Atribute Navigation -->
         
                  <div class="no_sticky">
                   <div class="row">
                    <div class="topp-search">
                        <div class="input-group" style="width: 100%;">
                            <form action="{{ route('search') }}" method="GET">
                                @csrf
                                <input type="text" id="search" name="query" class="form-control" placeholder="Search">
                                <button type="submit">
                                    <i class="fas fa-search"></i>
                                </button>  
                            </form>
                        </div>
                    </div>
                 </div>
                </div> 

                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="fa fa-bars"></i>
                    </button> -->
                    <a class="navbar-brand" href="#">
                        <img src="{{ asset('theme/images/logo-2.png') }}" class="logo" alt="Logo">
                    </a>
                </div>
                <!-- End Header Navigation -->

            </div>

        </nav>
        <!-- End Navigation -->
    </header>
    <!-- End Header -->

    @yield('content')



    <!-- Start Footer 
    ============================================= -->
    <footer class="bg-light">
        <!-- Start Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-3" style="width: 25%; text-align: center">
                    <ul class="fr-read">
                    <li><img src="{{ asset('theme/images/ico.png') }}"  /></li>  
                        <li style="text-transform: uppercase;">Read | Write | Research</li>  
                        <li>Scholar Nepal</li>  
                        <li>Digital Journals</li>  
                     </ul>   
                    </div>
                    <hr>
                    <div class="col-md-9 link">
                        <ul>

                        @foreach($pages as $page)   
                           <li>
                                <a href="{{ route('pages.show', $page->id) }}">{{ $page->title }}</a>
                            </li>
                        @endforeach    
                          
                             <li>
                                <a href="#">Contact</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="ti-facebook"></span>
                                    <span class="ti-twitter"></span>
                                    <span class="ti-instagram"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                   <!--  <div class="col-md-2 link">
                        <ul>
                            <li>
                                <a href="{{ route('login') }}">Login</a>
                            </li>
                            <li>
                                <a href="{{ route('register') }}">Register</a>
                            </li>
                          
                        </ul>
                    </div> -->
                    
                <!--      <div class="col-md-4">
                         <div class="newsletter-area">

                          <div style="height: 30px"></div>
                          <h3 style="font-size: 21px; font-weight: normal">
                               Subscribe to our newsletter</h3>
                            <form action="#">
                                <div class="input-group">
                                    <input type="email" placeholder="Enter your e-mail here" class="form-control" name="email">
                                    <button type="submit">
                                        Subscribe <i class="fa fa-paper-plane"></i>
                                    </button>  
                                </div>
                            </form>      
                        </div>
                       </div> -->


                     </div>
                     <p 
                      style="text-align: center; 
                             font-size: 13px;
                             color: #333;
                            padding: 45px 0 0px 0">
                   Copyright © Scholar Nepal <?php echo date("Y")?>. All Rights Reserved.
                </p>
                   </div>

                </div>

            </div>


        </div>
        <!-- End Footer Bottom -->
        <!-- <p style="text-align: center; padding: 25px 0 10px 0">
           Copyright © Scholar Nepal <?php echo date("Y")?>. All Rights Reserved.
        </p>
 -->    </footer>
    <!-- End Footer -->

<style type="text/css">
    .link ul li{
        float: left !important; display: inline-block !important;
        padding:0 13px;
    }
/*    footer .footer-bottom {
        padding: 30px 0 10px 0 !important;
    }*/
    .fr-read img {
        opacity: 1 !important;
       
    }
    .fr-read li {
       color: #000 !important;  
    }
    footer .footer-bottom .link li a {
        color: #000 !important;  
    }
    .no_sticky{
        float: left;
        width: 30%;
        margin: 1.3% 0 0 10%;
    }
   
</style>


    <!-- jQuery Frameworks
    ============================================= -->
    <script src="{{ asset('theme/assets/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/equal-height.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/jquery.appear.js')}}"></script>
    <script src="{{ asset('theme/assets/js/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/modernizr.custom.13711.js')}}"></script>
    <script src="{{ asset('theme/assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/wow.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/progress-bar.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/count-to.js')}}"></script>
    <script src="{{ asset('theme/assets/js/YTPlayer.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/loopcounter.js')}}"></script>
    <script src="{{ asset('theme/assets/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/bootsnav.js')}}"></script>
    <script src="{{ asset('theme/assets/js/main.js')}}"></script>
    
  

    @yield('scripts')

</body>

</html>