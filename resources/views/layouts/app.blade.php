<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
      @yield('title')
    </title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
<!-- ========== Google Fonts ========== -->
<link href="{{ asset('theme/assets/css/font-awesome.min.css') }}" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />

<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Girassol&display=swap" rel="stylesheet"> 
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css">
      .btn-info{
        color: #fff;
      }
    </style>
    @yield('css')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel" style="position: fixed; width: 100%; z-index: 99; top: 0px; padding: 0px;">

            <div class="container">
                 <div class="attr-nav inc-btn"  style="float: left;">
                   <a class="navbar-brand" href="/"  style="display: block;">
                        <img src="{{ asset('theme/images/logo2.png') }}" class="logo" alt="Logo" style="width: 26% !important;">
                    </a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                                @if(!empty(Auth::user()->image))

                                <img style=" margin-right: 10px; width: 30px; height: 30px; border-radius: 50%" src="{{asset('')}}data/{{ Auth::user()->image}}" 
                                    alt="{{ Auth::user()->fname }} {{ Auth::user()->lname }}"/>
                                @else
                                <img style=" margin-right: 10px; width: 30px; height: 30px; border-radius: 50%" src="{{ Gravatar::src(Auth::user()->lname) }}" alt="{{ Auth::user()->fname }} {{ Auth::user()->lname }}"/>
                                @endif


                                    {{ Auth::user()->fname }}  {{ Auth::user()->lname }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('home') }}">
                                    <i class="fa fa-pie-chart" aria-hidden="true"></i>
                                        Dashboard
                                    </a>
                                    <a class="dropdown-item" href="{{ route('users.edit-profile') }}">
                                       <i class="fa fa-user" aria-hidden="true"></i>
                                        Profile
                                    </a>
                                    
                                     <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-lock" aria-hidden="true"></i>
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>


<div style="clear: both; height: 60px;"></div>
 
        <main style="margin-top: 35px;">

         @auth
             <div class="container">
                @if(session()->has('success'))
                  <div class="alert alert-success">
                   {{ session()->get('success') }}
                  </div>
                @endif
             </div> 

             <div class="container">
                @if(session()->has('error'))
                  <div class="alert alert-danger">
                   {{ session()->get('error') }}
                  </div>
                @endif
             </div>  

             <div class="container">
               <div class="row">
                  <div class="col-md-3">
                     <ul class="list-group">

                   <!-- @If(auth()->user()->isAdmin()) -->

                   <!--   <li class="list-group-item">
                           <a href="{{ route('users.index') }}">
                             <i class="fa fa-users" aria-hidden="true"></i>
                             Users
                           </a>
                     </li> -->

                <!--    @endif   -->
                    <ul class="list-group my-2" style="margin-top:0px !important;">
                       <li class="list-group-item">
                           <a href="{{ route('users.edit-profile') }}">
                             <i class="fa fa-product-hunt" aria-hidden="true"></i>
                               My Profile
                           </a>
                       </li>
                    </ul> 
                     <li class="list-group-item">
                           <a href="{{ route('tags.index') }}">
                             <i class="fa fa-tag" aria-hidden="true"></i>
                             Tags
                           </a>
                       </li>


                       <li class="list-group-item">
                           <a href="{{ route('posts.index') }}">
                              <i class="fa fa-paperclip" aria-hidden="true"></i>
                              Posts
                           </a>
                       </li>

                       
                       <li class="list-group-item">
                           <a href="{{ route('categories.index') }}">
                             <i class="fa fa-th-list" aria-hidden="true"></i>
                             Categories
                           </a>
                       </li>
                     </ul> 

                     <ul class="list-group my-2">
                       <li class="list-group-item">
                           <a href="{{ route('papers.index') }}">
                              <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                               Papers
                           </a>
                       </li>
                    </ul>      

                     <ul class="list-group my-5">
                       <li class="list-group-item">
                           <a href="{{ route('trashed-posts.index') }}">
                              <i class="fa fa-trash" aria-hidden="true"></i>
                              Bin
                           </a>
                       </li>
                     </ul>                  
                  </div>
      
                  <div class="col-md-9">
                       @yield('content')
                  </div>
               </div>
             </div>
         @else
           @yield('content')
         @endauth


<div style="height: 45px"></div>
       <footer class="bg-light">
        <!-- Start Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-3" style="width: 33%">
                    <ul class="fr-read">
                    <li><img src="{{ asset('theme/images/ico.png') }}" /></li>  
                        <li style="text-transform: uppercase;">Read | Write | Research</li>  
                        <li>Scholar Nepal</li>  
                        <li>Digital Journals</li>  
                     </ul>   
                    </div>
                    <hr>
                    <div class="col-md-3 link">
                        <ul>
                        @foreach($pages as $page)   
                           <li>
                                <a href="{{ route('pages.show', $page->id) }}">{{ $page->title }}</a>
                            </li>
                        @endforeach 
                            <li>
                                <a href="#">
                                    <span class="ti-facebook"></span>
                                    <span class="ti-twitter"></span>
                                    <span class="ti-instagram"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-2 link">
                        <ul>
                           <li>
                                <a href="{{ route('login') }}">Login</a>
                            </li>
                            <li>
                                <a href="{{ route('register') }}">Register</a>
                            </li>
                          
                        </ul>
                    </div>
                    
                     <div class="col-md-4">
                         <div class="newsletter-area">

                          <div style="height: 30px"></div>
                          <h3 style="font-size: 21px; font-weight: normal">
                               Subscribe to our newsletter</h3>
                            <form action="#">
                                <div class="input-group">
                                    <input type="email" placeholder="Enter your e-mail here" class="form-control" name="email">
                                    <button type="submit">
                                        Subscribe <i class="fa fa-paper-plane"></i>
                                    </button>  
                                </div>
                            </form>      
                        </div>
                       </div>


                     </div>
                     <p 
                      style="text-align: center; 
                             font-size: 13px;
                             color: #333;
                            padding: 45px 0 0px 0">
                   Copyright © Scholar Nepal <?php echo date("Y")?>. All Rights Reserved.
                </p>
                   </div>

                </div>

            </div>


        </div>
        <!-- End Footer Bottom -->
        <!-- <p style="text-align: center; padding: 25px 0 10px 0">
           Copyright © Scholar Nepal <?php echo date("Y")?>. All Rights Reserved.
        </p>
 -->    </footer>

        </main>
    </div>


<!--     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
   
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.min.js" crossorigin="anonymous"></script> -->
<!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scripts')

    <style type="text/css">
      /*.link ul li{
        list-style: none;
        text-align: left; float: left; 
        padding: 0 10px;
      }
*/      .fa{
        width:  20px;
        color: #ffc41c;
      }
      .list-group-item a{
        font-size: 15px !important;
        color: #999;
      }
      .list-group-item a:hover{
        color: #000;
        text-decoration: unset;
        }
    </style>
    
</body>
</html>
