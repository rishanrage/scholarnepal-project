@extends('layouts.main')

@section('keywords')
Digital Journal in Nepal
@endsection

@section('description')
Digital Journal in Nepal, publish your paper
@endsection


@section('title')
  Scholar Nepal :: Digital Journal
@endsection


@section('content')
   <div class="banner-area shadow bg-fixed dark text-center text-light" style="background-image: url({{ asset('theme/images/banner-2.jpg') }}); height: 270px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="box-table">
                        <div class="box-cell">
                            <div class="content search-content" style="display: none;">
                                <h2>Your Digital Journals</h2>                      
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="row">
                                        <form action="#">
                                            <input type="text" placeholder="Enter Keywords" class="form-control" name="text">
                                            <button type="submit">
                                                <i class="ti-search"></i> Search 
                                            </button>  
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->


<div class="weekly-top-items carousel-shadow" style="padding-bottom: 0px;" >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                 <div class="top-courses">
                    <div class="heading" style="padding: 0px;">
                      <!-- <h4>Announcements</h4> -->


<div class="acme-news-ticker">
    <!--<div class="acme-news-ticker-label">      -->
   <!--    <span class="ti-announcement" 
            style="color: #a19c8f;
            font-size: 25px;"></span> -->

    <!--         <i class="fa fa-bullhorn" aria-hidden="true"
            style="color: #ffc41c;
            font-size: 25px;"></i> -->
    <!--        <img style="width: 50px; opacity: .5" -->
    <!--             src="{{ asset('theme/images/news.png') }}" alt="Announcements">-->

    <!--</div>-->

    <div class="acme-news-ticker-box">
        
        <ul class="my-news-ticker">
        @foreach($news as $n)
            <li class="my-news-ticker_li">
              <a href="{{ route('news.show', $n->id) }}">{{ $n->title }} </a>
            </li>
        @endforeach    
        </ul>

    </div>
    <div class="acme-news-ticker-controls acme-news-ticker-horizontal-controls">
        <button class="acme-news-ticker-arrow acme-news-ticker-prev"></button>
        <!--<button class="acme-news-ticker-pause"></button>-->
        <button class="acme-news-ticker-arrow acme-news-ticker-next"></button>
    </div>
</div>   


                    </div>
                   </div>
                </div>
            </div> 
        </div>
     </div>  

<div class="container">
    <hr>
</div>      
    <!-- Start Weekly Top
    ============================================= -->
    <div class="weekly-top-items carousel-shadow default-paddingg" >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="top-courses">
                        <div class="heading">
                            <!-- <h4>Top Courses - <strong>Weekly</strong></h4> -->
                             <h4>Trending</h4>
                        </div>
                        <div class="top-course-items top-courses-carousel owl-carousel owl-theme">
                            <!-- Single Item -->
                            @foreach($posts as $post)
                            <div class="item">
                                <div class="thumb">
                                    <img src="{{ asset('data/'.$post->image) }}" alt="{{ $post->title }}" style="height: 205px !important;">
                                    <div class="overlay">
                                        <ul>
                                            <li>
                                            @if($post->views > 0)
                                              <span class="ti-eye"></span> 
                                               <span style="font-size: 11px;color: #ffc41c;">{{ $post->views }} Views</span><br/>

                                            @endif
                                               <a href="{{ route('blog.show', $post->id) }}">{{ $post->title }}</a> 

                                            </li>
                                        
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!-- Single Item -->

                        </div>
                    </div>
                </div>
         
            </div>
        </div>
    </div>
    <!-- End Weekly Top -->

    <!-- Start Categories 
    ============================================= -->
    <div class="category-area  default-padding" style="padding-bottom: 10px; padding-top: 0px;">
        <div class="container">
            <div class="row">
                <div class="site-heading">
                    <div class="col-md-8">
                        <h4 style="float: left;">Categories</h4>
                    </div>
                    <div class="col-md-4">
                       <a href="#" class="v-all-btn">View All</a>
                   </div>
                </div>
            </div>
            <hr>
            <div class="category-items">
                <div class="row">
                    <!-- Single Item -->

                    @foreach($categories as $category)
                        <div class="col-md-4 col-sm-6 equal-height">
                            <div class="item mariner">
                                <a href="{{ route('paper.category', $category->id) }}">
                                    <div class="item-box">
                                       <!--  <div class="icon">
                                   
                                            <i class="flaticon-algorithm"></i>
                                        </div> -->
                                        <div class="info">
                                            <h5>{{ $category->name }}</h5>
                                            <p>
                                                Paper Published <strong>
                                                @php
                                                 $papers = \App\Paper::where('category_id', $category->id)->get();
                                                @endphp    
                                                  {{ $papers->count() }}
                                              
                                                </strong>
                                            </p>
                                            <span>
                                                @php
                                                 $uploads = \App\Upload::where('category_id', $category->id)->get();
                                                @endphp  
                                                {{ $uploads->count() }}  
                                             Downloads</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                      @endforeach  
                    <!-- End Single Item -->
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End Categories -->
    
     <!-- Start Weekly Top
    ============================================= -->
    <div class="weekly-top-items carousel-shadow default-paddingg">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="top-courses">
                        <ul class="nav nav-tabs">
                         <h4 class="frm-archives">From the Archives</h4>

                        @foreach($featured as $feat)
                          <li class="{{ $loop->first ? 'active' : '' }}">
                             <a data-toggle="tab" href="#featured{{ $feat->id }}">
                               {{ $feat->name }}
                             </a>
                          </li>
                        @endforeach  

                        </ul>


<div class="tab-content" 
     style="padding:20px 20px 20px 0;">


@foreach($featured as $postFeat)

  <div id="featured{{ $postFeat->id }}" class="tab-pane fade {{ $loop->first ? 'in active' : '' }}">

@php
 $apple = \App\Post::where('category_id', $postFeat->id)->orderBy('id', 'desc')->get()
@endphp  


@foreach($apple as $key => $a)
  @if($key == 0)
   <div class="col-md-6" style="padding-left:0;">
        <div class="item">
           <div class="thumb">
               <img src="{{ asset('data/'.$a->image) }}" 
                    alt="{{ $a->title }}" 
                    style="height: 200px; border-radius: 4px; margin-top: 5px;">
                      </div>
                         <div class="info">
                            <h4 style="margin-top: 10px; font-size: 14px; line-height: 20px; margin-bottom: 0px !important;">
                              <a href="{{ route('blog.show', $a->id) }}">
                                {{ str_limit($a->title,60) }}</a>
                              <p style="font-size: 13px;  margin-bottom: 0px !important;">
                              <i class="fas fa-calendar-alt"></i>                   
                              {{ date('F j, Y', strtotime($a->published_at)) }}
                                </p>
                                 </h4>
                                 <p>{{ str_limit($a->description,100) }}</p>
                                </div>
                            </div>
                         </div>
    @endif                     
  @endforeach                       

  <div class="col-md-6 col-sm-6 item equal-height" style="height: 378px;">
    <div class="f-item popular-courses">
        <ul>

     @foreach($apple as $key => $a)
      @if($key > 0 && $key < 5)   
          <li>
            <div class="thumb">
              <a href="{{ route('blog.show', $a->id) }}">
               <img src="{{ asset('data/'.$a->image) }}"
                    alt="{{ $a->title }}">
              </a>
            </div>
            <div class="info">
               <a href="{{ route('blog.show', $a->id) }}">
                {{ str_limit($a->title,50) }}
              </a>
                 <p style="font-size: 13px;">
                   <i class="fas fa-calendar-alt"></i> 
                  {{ date('F j, Y', strtotime($a->published_at)) }}</p>
            </div>
          </li>  
       @endif                     
     @endforeach  

         </ul>
     </div>
  </div>

  </div>

@endforeach
  
         </div>
     </div>
</div>

  <div class="col-md-4 tags">
     <h4 class="frm-archives" 
         style="padding-bottom: 10px; border-bottom: 1px solid #ccc; 
                 width: 100%; margin-bottom: 20px;">Trending topics</h4>
            @foreach($tags as $tag)
              <a href="{{ route('blog.tag', $tag->id) }}">@ {{ $tag->name }}</a>
            @endforeach   
             </div> 


            </div>
        </div>
    </div>
   
    <style type="text/css">
    .dropdown-item{
      display: block;
    }
    .dropdown-item:hover{
      color: #666;
    }
    .f-item li {
      margin-bottom: 0px !important;
    }
    .f-item.popular-courses li .info{
      line-height: 21px !important;
    }
 /*   .dropdown-menu{
      padding: 15px !important;
    }*/
    .my-news-ticker_li {
        text-align: center !important; width: 100% !important;
    }
    
    .acme-news-ticker-next {
    position: relative !important;
    left: 1030px !important;
   }
   
   .acme-news-ticker-controls {
       left: 0px !important;
   }
   
  </style>

@endsection

@section('scripts')
<script type='text/javascript' src='{{ asset('theme/assets/js/ticker.js')}}'></script>

    <script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.my-news-ticker').AcmeTicker({
            type:'horizontal',/*horizontal/horizontal/Marquee/type*/
            direction: 'right',/*up/down/left/right*/
            controls: {
                prev: $('.acme-news-ticker-prev'),/*Can be used for horizontal/horizontal/typewriter*//*not work for marquee*/
                toggle: $('.acme-news-ticker-pause'),/*Can be used for horizontal/horizontal/typewriter*//*not work for marquee*/
                next: $('.acme-news-ticker-next')/*Can be used for horizontal/horizontal/marquee/typewriter*/
            }
        });
    })

</script>



@endsection

