<!DOCTYPE html>
<html lang="en">
<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Scholar Nepal :: Digital Journal">

    <!-- ========== Page Title ========== -->
    <title>Scholar Nepal :: Digital Journal</title>

    <!-- ========== Favicon Icon ========== -->
    <link rel="shortcut icon" href="{{ asset('theme/images/ico.png') }}" type="image/x-icon">

    <!-- ========== Start Stylesheet ========== -->
    <link href="{{ asset('theme/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('theme/assets/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('theme/assets/css/flaticon-set.css') }}" rel="stylesheet" />
    <link href="{{ asset('theme/assets/css/themify-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('theme/assets/css/magnific-popup.css') }}" rel="stylesheet" />
    <link href="{{ asset('theme/assets/css/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('theme/assets/css/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('theme/assets/css/animate.css') }}" rel="stylesheet" />
    <link href="{{ asset('theme/assets/css/bootsnav.css') }}" rel="stylesheet" />
    <link href="{{ asset('theme/style.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/assets/css/responsive.css') }}" rel="stylesheet" />

    <!-- ========== Google Fonts ========== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Girassol&display=swap" rel="stylesheet"> 

</head>

<body>

    <!-- Preloader Start -->
    <div class="se-pre-con"></div>
    <!-- Preloader Ends -->

    <!-- Header 
    ============================================= -->
    <header id="home">

        <!-- Start Navigation -->
        <nav class="navbar navbar-default logo-less small-pad navbar-sticky bootsnav">

            <!-- Start Top Search -->
            <div class="container">
                <div class="row">
                    <div class="top-search">
                        <div class="input-group">
                            <form action="#">
                                <input type="text" name="text" class="form-control" placeholder="Search">
                                <button type="submit">
                                    <i class="fas fa-search"></i>
                                </button>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container">

            <div class="attr-nav inc-btn">
                    <ul>
                        <li>
                            <a href="#">Sign Up</a>
                        </li>
                        <li>
                            <a href="#">Log In</a>
                        </li>
                    </ul>
                </div> 
                <!-- Start Atribute Navigation -->
                <div class="attr-nav inc-btn"  style="float: left;">
                   <a class="navbar-brand" href="#"  style="display: block;">
                        <img src="{{ asset('theme/images/logo-2.png') }}" class="logo" alt="Logo">
                    </a>
                </div>        
                <!-- End Atribute Navigation -->
         
                 <div class="top-sticky">
                   <div class="row">
                    <div class="topp-search">
                        <div class="input-group" style="width: 100%;">
                            <form action="#">
                                <input type="text" name="text" class="form-control" placeholder="Search">
                                <button type="submit">
                                    <i class="fas fa-search"></i>
                                </button>  
                            </form>
                        </div>
                    </div>
                 </div>
                </div> 

                

                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="fa fa-bars"></i>
                    </button> -->
                    <a class="navbar-brand" href="#">
                        <img src="{{ asset('theme/images/logo-2.png') }}" class="logo" alt="Logo">
                    </a>
                </div>
                <!-- End Header Navigation -->

            </div>

        </nav>
        <!-- End Navigation -->

    </header>
    <!-- End Header -->

    <!-- Start Banner 
    ============================================= -->
    <div class="banner-area shadow bg-fixed dark text-center text-light" style="background-image: url({{ asset('theme/images/banner-2.jpg') }}); height: 350px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="box-table">
                        <div class="box-cell">
                            <div class="content search-content">
                                <h2>Your Digital Journals</h2>                      
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="row">
                                        <form action="#">
                                            <input type="text" placeholder="Enter Keywords" class="form-control" name="text">
                                            <button type="submit">
                                                <i class="ti-search"></i> Search 
                                            </button>  
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->


<div class="weekly-top-items carousel-shadow" style="padding-bottom: 0px;" >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                 <div class="top-courses">
                    <div class="heading">
                      <h4>Announcements</h4>
                    </div>
                   </div>
                </div>
            </div> 
        </div>
</div>  

<div class="container">
    <hr>
</div>      
    <!-- Start Weekly Top
    ============================================= -->
    <div class="weekly-top-items carousel-shadow default-paddingg" >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="top-courses">
                        <div class="heading">
                            <!-- <h4>Top Courses - <strong>Weekly</strong></h4> -->
                             <h4>Trending</h4>
                        </div>
                        <div class="top-course-items top-courses-carousel owl-carousel owl-theme">
                            <!-- Single Item -->
                            @foreach($posts as $post)
                            <div class="item">
                                <div class="thumb">
                                    <img src="{{ asset('data/'.$post->image) }}" alt="{{ $post->title }}">
                                    <div class="overlay">
                                        <ul>
                                            <li><a href="#">{{ $post->title }}</a></li>
                                            <li><i class="fas fa-eye"></i> 18</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!-- Single Item -->

                        </div>
                    </div>
                </div>
         
            </div>
        </div>
    </div>
    <!-- End Weekly Top -->

    

<!-- <div class="container">
    <hr>
</div> -->

    <!-- Start Categories 
    ============================================= -->
    <div class="category-area  default-padding" style="padding-bottom: 10px; padding-top: 0px;">
        <div class="container">


            <div class="row">
                <div class="site-heading">
                    <div class="col-md-8">
                        <h4 style="float: left;">Categories</h4>
                    </div>
                    <div class="col-md-4">
                       <a href="#" class="v-all-btn">View All</a>
                   </div>
                </div>
            </div>
            <hr>
            <div class="category-items">
                <div class="row">
                    <!-- Single Item -->

                    @foreach($categories as $category)
                        <div class="col-md-4 col-sm-6 equal-height">
                            <div class="item mariner">
                                <a href="#">
                                    <div class="item-box">
                                       <!--  <div class="icon">
                                            <i class="flaticon-algorithm"></i>
                                        </div> -->
                                        <div class="info">
                                            <h5>{{ $category->name }}</h5>
                                            <p>
                                                Paper Published <strong>1278</strong>
                                            </p>
                                            <span>28 Downloads</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                      @endforeach  
                    <!-- End Single Item -->
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End Categories -->
    
     <!-- Start Weekly Top
    ============================================= -->
    <div class="weekly-top-items carousel-shadow default-paddingg">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="top-courses">

                        <ul class="nav nav-tabs">
                         <h4 class="frm-archives">From the Archives</h4>
                          <li class="active"><a data-toggle="tab" href="#science">Science</a></li>
                          <li><a data-toggle="tab" href="#business">Business</a></li>
                          <li><a data-toggle="tab" href="#people">People</a></li>
                        </ul>


<div class="tab-content" style="padding:20px 20px 20px 0;">

  <div id="science" class="tab-pane fade in active">

   <div class="col-md-6" style="padding-left:0;">
        <div class="item">
           <div class="thumb">
               <img src="{{ asset('theme/images/n-2.jpg') }}" alt="Thumb" style="height: 200px; border-radius: 4px; margin-top: 5px;">
                      </div>
                         <div class="info">
                            <h4 style="margin-top: 10px; font-size: 14px; line-height: 20px;">
                              <a href="#">Social Entrepreneurship in Chepang Community through Herbal Product</a>
                              <p style="font-size: 13px;"><i class="fas fa-calendar-alt"></i> 14 Jun, 2021</p>
                                 </h4>
                                 <p>
                                  Wisdom praise things she before. Be mother itself vanity favour do me of. Begin sex was power joy after had walls miles. 
                                    </p>
                                </div>
                            </div>

</div>

<div class="col-md-6 col-sm-6 item equal-height" style="height: 378px;">
                        <div class="f-item popular-courses">
                          
                            <ul>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="{{ asset('theme/assets/img/gallery/1.jpg') }}" alt="Thumb">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="#">Subjects allied to Creative arts and design</a>
                                      <p style="font-size: 13px;"><i class="fas fa-calendar-alt"></i> 14 Jun, 2021</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="{{ asset('theme/assets/img/gallery/3.jpg') }}" alt="Thumb">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="#">Strength and Weaknesses of Online Business</a>
                                      <p style="font-size: 13px;"><i class="fas fa-calendar-alt"></i> 14 Jun, 2021</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="{{ asset('theme/assets/img/gallery/4.jpg') }}" alt="Thumb">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="#">Business and administrative subjects</a>
                                         <p style="font-size: 13px;"><i class="fas fa-calendar-alt"></i> 14 Jun, 2021</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

  </div>


  <div id="business" class="tab-pane fade">
    <div class="col-md-6" style="padding-left:0;">
        <div class="item">
           <div class="thumb">
               <img src="{{ asset('theme/images/n-1.jpg') }}" alt="Thumb" style="height: 200px; border-radius: 4px; margin-top: 5px;">
                      </div>
                         <div class="info">
                            <h4 style="margin-top: 10px;font-size: 14px; line-height: 20px;">
                              <a href="#">Challenges and Opportunities of Renewable Energy in Nepal</a>
                              <p style="font-size: 13px;"><i class="fas fa-calendar-alt"></i> 14 Jun, 2021</p>
                                 </h4>
                                 <p>
                                  Wisdom praise things she before. Be mother itself vanity favour do me of. Begin sex was power joy after had walls miles. 
                                    </p>
                                </div>
                            </div>

</div>

<div class="col-md-6 col-sm-6 item equal-height" style="height: 378px;">
                        <div class="f-item popular-courses">
                          
                            <ul>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="assets/img/gallery/1.jpg" alt="Thumb">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="#">Subjects allied to Creative arts and design</a>
                                      <p style="font-size: 13px;"><i class="fas fa-calendar-alt"></i> 14 Jun, 2021</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="assets/img/gallery/3.jpg" alt="Thumb">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="#">Strength and Weaknesses of Online Business</a>
                                      <p style="font-size: 13px;"><i class="fas fa-calendar-alt"></i> 14 Jun, 2021</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="assets/img/gallery/4.jpg" alt="Thumb">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="#">Business and administrative subjects</a>
                                         <p style="font-size: 13px;"><i class="fas fa-calendar-alt"></i> 14 Jun, 2021</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
  </div>
  <div id="people" class="tab-pane fade">
    <div class="col-md-6" style="padding-left:0;">
        <div class="item">
           <div class="thumb">
               <img src="images/n-3.jpg" alt="Thumb" style="height: 200px; border-radius: 4px; margin-top: 5px;">
                      </div>
                         <div class="info">
                            <h4 style="margin-top: 10px;font-size: 14px; line-height: 20px;">
                              <a href="#">Effectiveness of Improved Cook Stove </a>
                              <p style="font-size: 13px;"><i class="fas fa-calendar-alt"></i> 14 Jun, 2021</p>
                                 </h4>
                                 <p>
                                  Wisdom praise things she before. Be mother itself vanity favour do me of. Begin sex was power joy after had walls miles. 
                                    </p>
                                </div>
                            </div>

</div>

<div class="col-md-6 col-sm-6 item equal-height" style="height: 378px;">
                        <div class="f-item popular-courses">
                          
                            <ul>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="assets/img/gallery/1.jpg" alt="Thumb">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="#">Subjects allied to Creative arts and design</a>
                                      <p style="font-size: 13px;"><i class="fas fa-calendar-alt"></i> 14 Jun, 2021</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="assets/img/gallery/3.jpg" alt="Thumb">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="#">Strength and Weaknesses of Online Business</a>
                                      <p style="font-size: 13px;"><i class="fas fa-calendar-alt"></i> 14 Jun, 2021</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="assets/img/gallery/4.jpg" alt="Thumb">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <a href="#">Business and administrative subjects</a>
                                         <p style="font-size: 13px;"><i class="fas fa-calendar-alt"></i> 14 Jun, 2021</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
  </div>
</div>


                    </div>
                </div>

                <div class="col-md-4 tags">
                     <h4 class="frm-archives" style="padding-bottom: 10px; border-bottom: 1px solid #ccc; width: 100%; margin-bottom: 20px;">Tags</h4>

                     <a href="#">#Audit</a>
                      <a href="#">#Business </a>
                       <a href="#">#Innovation </a>
                        <a href="#">#lockdown </a>
                         <a href="#">#Electricity </a>
                          <a href="#">#Entrepreneurship </a>
                           <a href="#">#Coronavirus </a>
                            <a href="#">#COVID-19</a>
                             <a href="#">#Heritage </a>
                              <a href="#">#Health </a>
                               <a href="#">#Commerce</a>
                                <a href="#">#Audit</a>
                      <a href="#">#Hydropower  </a>
                       <a href="#">#Tribhuvan University </a>
                        <a href="#">#Education </a>
                         <a href="#">#Crypto </a>
                          <a href="#">#Currrency </a>
                           <a href="#">#BitCoin </a>
                            <a href="#">#Power</a>
                            <!--  <a href="#">#Renewal </a>
                              <a href="#">#Energy </a>
                               <a href="#">#Commerce</a> -->
                </div>
            
            </div>
        </div>
    </div>
    <!-- End Weekly Top -->

    


    <!-- Start Newsletter 
    ============================================= -->
    <div class="newsletter-area" style="padding-top: 10px; padding-bottom: 40px;">
        <div class="container">
            <div class="subscribe-items shadow theme-hard default-padding bg-cover" style="background-image: url(assets/img/banner/11.jpg);">
                <div class="row">
                    <div class="col-md-7 left-info">
                        <div class="info-box">
                            <div class="icon">
                                <i class="flaticon-email"></i>
                            </div>
                            <div class="info">
                                <h3>Subscribe to our newsletter</h3>
                                <p>
                                    Prospect humoured mistress to by proposal marianne attended. Simplicity the far admiration preference everything.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <form action="#">
                            <div class="input-group">
                                <input type="email" placeholder="Enter your e-mail here" class="form-control" name="email">
                                <button type="submit">
                                    Subscribe <i class="fa fa-paper-plane"></i>
                                </button>  
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Newsletter -->

    <!-- Start Footer 
    ============================================= -->
    <footer class="bg-light">
        
        <!-- Start Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>&copy; Copyright 2021. All Rights Reserved by <a href="#"> Scholar Nepal</a></p>
                    </div>
                    <div class="col-md-6 text-right link">
                        <ul>
                            <li>
                                <a href="#">About</a>
                            </li>

                            <li>
                                <a href="#">Faqs</a>
                            </li>
                            <!-- <li>
                                <a href="#">License</a>
                            </li> -->
                            <li>
                                <a href="#">Disclaimer</a>
                            </li>
                             <li>
                                <a href="#">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Footer Bottom -->
    </footer>
    <!-- End Footer -->

    <!-- jQuery Frameworks
    ============================================= -->
    <script src="{{ asset('theme/assets/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('theme/assets/js/equal-height.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/jquery.appear.js')}}"></script>
    <script src="{{ asset('theme/assets/js/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/modernizr.custom.13711.js')}}"></script>
    <script src="{{ asset('theme/assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/wow.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/progress-bar.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/count-to.js')}}"></script>
    <script src="{{ asset('theme/assets/js/YTPlayer.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/loopcounter.js')}}"></script>
    <script src="{{ asset('theme/assets/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('theme/assets/js/bootsnav.js')}}"></script>
    <script src="{{ asset('theme/assets/js/main.js')}}"></script>

</body>

</html>