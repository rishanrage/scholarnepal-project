@extends('layouts.main')

@section('title')
 {{ $page->title }} | Scholar Nepal :: Digital Journal
@endsection


@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-10" style="padding-left: 5%">

    <h1 class="my-5" style="border-bottom: 1px solid; line-height: 54px;
                            padding-bottom: 15px;">{{ $page->title }}
                            </h1>

  @if(!empty($page->description))
    <div class="card">
      <div class="shadow-sm">
        {!! $page->description !!}
      </div>  
    </div> 
  @endif         

   @if(!empty($page->image))
      <img src="{{ asset('data/'.$page->image ) }}" /> 
    @endif                       

<div style="height: 25px"></div> 
     {!! $page->content !!}
     
     </div>
   </div>
</div>

<style type="text/css">
	.shadow-sm {
    box-shadow: 0 .125rem .25rem rgba(0,0,0,.075) !important;
    box-shadow: 0 .125rem .25rem rgba(0,0,0,.075) !important;
	padding: 25px;
	margin: 65px;
	font-size: 18px;
	line-height: 30px;
	background: #f1f1f1;
}
p{
	font-size: 17px;
	line-height: 29px;
}
 .dropdown-item{
      display: block;
    }
    .dropdown-item:hover{
      color: #666;
    }
</style>

@endsection