@extends('layouts.app')

@section('title')
  Scholar Nepal | Papers
@endsection

@section('content')

<div class="d-flex justify-content-end mb-2">
 <a href="{{ route('papers.upload') }}" class="btn btn-success">Upload Paper</a>
</div>

<div class="card card-default">
  <div class="card-header">
     Papers <span class="badge bg-warning">{{ $papers->count() }}</span> 
  </div>

  <div class="card-body">
    @if($papers->count()>0)
     <table class="table">
      <thead> 
        <th>Published Date</th>
        <th style="width: 45%">Title</th> 
        <th>Paper Count</th>       
        <th>Action</th>
      </thead>
      <tbody>
       @foreach($papers as $paper)
	        <tr>
	          <td>{{ $paper->published_at }}</td>
	          <td>{{ $paper->title }} 
	            <!--   <span class="badge bg-info">
	                {{ $paper->upload->count() }}
	               </span>  -->
	          </td>
	          <td>{{ $paper->upload->count() }}</td>
	          <td>
	          	<a href="{{ route('papers.edit', $paper->id) }}" class="btn btn-info btn-info">Edit</a>
	            <button class="btn btn-info btn-danger" onclick="handleDelete({{ $paper->id }})">Delete</button>
	          </td>
	        </tr>
        @endforeach
      </tbody>   	
     </table> 
	@else
     <h6 class="text-center">No Paper yet</h6>
    @endif


    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog">

        <form action="" method="POST" id="deletepaper">
           @method('DELETE')
           @csrf          
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Delete Paper</h5>
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">
                <p class="text-center">Are you sure you want to delete this Paper?</p>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No, Go back</button>
                <button type="submit" class="btn btn-danger">Yes, Delete</button>
              </div>
            </div>
          </form>

      </div>
    </div>


  </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
  
  function handleDelete(id) {    
    var form = document.getElementById('deletepaper')
    form.action = '/papers/' + id
    console.log('deleting.', form);
    $('#deleteModal').modal('show')
  }

</script>

@endsection