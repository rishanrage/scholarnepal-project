@extends('layouts.app')

@section('title')
  Scholar Nepal | Upload Paper
@endsection

@section('content')

<div class="card card-default">
  <div class="card-header">
    Upload Paper
  </div>

  <div class="card-body">
    
    @include('partials.errors')
  
    <form action="" method="POST" enctype="multipart/form-data">   
      @csrf

      <div class="form-group row">
         <div class="col-md-6">
            <label for="published_at">Published Date</label>
            <input type="text" name="published_at" id="published_at" 
            class="form-control" value="{{ $paper->published_at }}">
          </div> 

          <div class="col-md-6">
            <label for="category">Category</label>
              <select name="category_id" id="category" class="form-control cat_selector">
              <option>Select Category</option>
                  @foreach($categories as $category)
                    <option value="{{ $category->id }}"
                      @if(isset($paper))
                          @if($category->id === $paper->category_id)
                            selected 
                          @endif
                        @endif 
                      >
                    {{ $category->name }}
                  </option>
                @endforeach  
              </select>
          </div>
      </div>

      <div class="form-group row">
         <div class="col-md-6">
            <label for="published_in">Published in (Name of Journal, Magazine, Report)</label>
            <input type="text" name="published_in" id="published_in" class="form-control" value="{{ $paper->published_in }}">
          </div> 

          <div class="col-md-6">
            <label for="doi">DOI or HTTP of original post</label>
            <input type="text" name="doi" id="doi" class="form-control" value="{{ $paper->doi }}">
          </div> 
      </div>

      <div class="form-group">
        <label for="keywords">Keywords</label>
        <input type="text" name="keywords" id="keywords" class="form-control" value="{{ $paper->keywords }}">
      </div>


    <div class="form-group row">
       <div class="col-md-12" style="margin-top: 5px;">  
        <label for="author">Author</label>
        </div>
        
        <div class="field_wrapper col-md-12">
         <div>
           
              <input type="text" name="fname[]" id="fname" class="col-md-3 form-control1" placeholder="First Name">
              <input type="text" name="mname[]" id="mname" class="col-md-3 form-control1"  placeholder="Middle Name">
              <input type="text" name="lname[]" id="lname" class="col-md-3 form-control1"  placeholder="Last Name">
         
            <a href="javascript:void(0);" class="add_button col-md-1" title="Add field">
              <i class="fa fa-plus-square-o" aria-hidden="true" style="font-size: 30px;"></i>
            </a>
       
          </div> 
        </div>   
          
      </div>


      <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" id="title" class="form-control" value="{{ $paper->title }}">
      </div>

      <div class="form-group">
       <label for="body">Abstract</label>
        <textarea name="body" id="body" class="form-control" cols="5" rows="2">
          {{ $paper->body }}
        </textarea>
      </div>

      <div class="form-group">
       <label for="upload" style="display: block; ">
             <span style="font-size: 12px;">Existing Uploads</span>
       </label>

      @foreach($uploads as $upload)
        <a href="{{ asset('data/papers/'.$upload->name) }}" style="font-size: 30px; margin-right: 10px;" target="_blank">
          <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
        </a>
      @endforeach 
      </div>

       <div class="form-group">
       <label for="upload">Upload</label>
        <input type="file" name="upload[]" id="upload" class="form-control" value="" multiple>
      </div>

      <!-- <div class="form-group">
         <button type="submit" class="btn btn-success">
           Upload Paper 
         </button>
      </div> -->

    </form>
  </div> 

</div>

@endsection

@section('scripts')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript">
  flatpickr('#published_at', 
   @if(isset($paper))
   {
    
   }
   @else
   {
    defaultDate: "today"
   }
   @endif
 )

  $(document).ready(function() {
      // $('.tags-selector').select2();
      $('.cat_selector').select2();
  })

</script>

<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div><input type="text" name="fname[]" id="fname" class="col-md-3 form-control2" placeholder="First Name"><input type="text" name="mname[]" id="mname" class="col-md-3 form-control2"  placeholder="Middle Name"><input type="text" name="lname[]" id="lname" class="col-md-3 form-control2"  placeholder="Last Name"><a href="javascript:void(0);" class="remove_button"><i class="fa fa-minus-square-o" aria-hidden="true" style="font-size: 30px;margin-left: 11px;"></i></a></div> '; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>

@endsection

@section('css')

<!--  <link href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css" rel="stylesheet"> -->
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
 <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
 <style type="text/css">
  .form-control1 {
    display: inline;
    max-width: 28%;
    height: calc(2.19rem + 2px);
    padding: .375rem .75rem;
    padding: 24px; margin-right: 10px; margin-bottom: 10px;
    font-size: .9rem;
    line-height: 1.6;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
}
 .form-control2 {
    display: inline;
    max-width: 28%;
    height: calc(2.19rem + 2px);
    padding: .375rem .75rem;
    padding: 24px; margin-right: 15px;
    font-size: .9rem;
    line-height: 1.6;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box; margin-bottom: 10px;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
}
</style>
@endsection