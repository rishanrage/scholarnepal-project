@extends('layouts.main')

@section('keywords')
{{ $paper->keywords }}
@endsection

@section('description')
{{ $paper->body }}
@endsection

@section('title')
  {{ $paper->title }} | Scholar Nepal :: Digital Journal 
@endsection

@section('content')

  <div class="container">
    <div class="row">
    <div class="col-md-10" style="padding-left:5%;">
    <h1 class="my-5" style="border-bottom: 1px solid; line-height: 54px;
                            padding-bottom: 10px;">{{ $paper->title }}
                            </h1>

    <a href="{{ route('paper.category',  $paper->category_id) }}" 
      style="text-transform: uppercase;">
         {{\App\Category::findOrFail($paper->category_id)->name}}  
      
      </a>

    <div class="course-meta">

     <p>
	     <span class="opacity-70 r-1">Contributor</span>
	     <a href="{{ route('blog.writer', $paper->user_id) }}" class="text-white"> 
	      {{\App\User::findOrFail($paper->user_id)->fname}} 
        {{\App\User::findOrFail($paper->user_id)->lname}} 
	     </a>
     </p>

<p style="margin-bottom: 10px; font-size: 17px;">
{{ $paper->body }}
</p>

<div style="clear: both; height: 15px"></div>

  @foreach($uploads as $upload)
     <a style="margin: 15px 10px 0 0" href="{{ asset('data/papers/'.$upload->name) }}" target="blank" title="{{ $upload->name }}">
     <i class="fa fa-file-pdf-o" aria-hidden="true" 
        style="font-size: 40px; color: red; margin-left: 5px;"></i>
     </a> 
  @endforeach

  @php     
   $author = App\Author::where('paper_id',$paper->id)->get();   
  @endphp

<div style="clear: both; height: 18px"></div>

<p style="margin-bottom: 0px;">
Published In <br/>
@if(!empty($paper->published_in))
<p style="color: #002147;">{{ $paper->published_in }}</p> 
 @else
 N\A
 @endif
</p>

<p>
Originally Posted In <br/>
 <a href="{{ $paper->doi }}" target="blank">
 @if(!empty($paper->doi))
 {{ $paper->doi }}
 @else
 N\A
 @endif
</a> 
</p>


<p style="margin-bottom: 0px;">
Published Date <br/>
@if(!empty($paper->published_at))
<p style="color: #002147;">{{ $paper->published_at }}</p> 
 @else
 N\A
 @endif
</p>


@if($author->count() > 0)
<span style="font-size: 14px;">

   @if($author->count() > 1) 
       Authors :
   @else
       Author :
   @endif        

</span>
@endif  



@foreach($author as $a)
<p style="margin-bottom: 0px; color: #002147;">
{{ $a->fname }} {{ $a->mname }} {{ $a->lname }} 
</p>
@endforeach



     
 <div style="height: 25px;"></div>
          
         </div>
       </div>
    </div>
  </div>


<style type="text/css">
  .dropdown-item{
    display: block;
  }
  .dropdown-item:hover{
    color: #666;
  }
 /* .dropdown-menu{
    padding: 15px !important;
  }*/
/*  body{
    font-size: 16px !important;
  }*/
</style>

@endsection



