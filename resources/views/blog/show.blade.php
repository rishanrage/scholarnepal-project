@extends('layouts.main')

@section('keywords')
{{ $post->keywords }}
@endsection

@section('description')
{{ $post->description }}
@endsection

@section('title')
 {{ $post->title }} | Scholar Nepal :: Digital Journal
@endsection


@section('content')


  <div class="container">
    <div class="row">
    <div class="col-md-10" style="padding-left:5%">
    <h1 class="my-5" style="border-bottom: 1px solid; line-height: 54px;
                            padding-bottom: 15px;">{{ $post->title }}
                            </h1>

    <a href="{{ route('blog.list', $post->category->id) }}" 
      style="text-transform: uppercase;">{{ $post->category->name }}</a>

    <div class="course-meta">
      <div class="item author">

              <div class="align-right">
                    @guest
                        <a href="{{ route('login') }}"
                          class="btn btn-theme effect btn-sm" 
                          style="color: #fff !important;">
                          <i class="fa fa-bookmark" aria-hidden="true"></i>
                            Follow
                        </a> 
                        
                      @else

                          @if(Auth::user()->id != $post->user->id)
                            @php
                                $user = App\User::findOrFail(Auth::user()->id);
                                $checkUser = App\User::findOrFail($post->user->id);
                            @endphp

                              @if($user->isFollowing($checkUser)) 

                                 <form action="{{ route('user.unfollow', $post->user->id) }}" 
                                       method="POST">
                                   @csrf
                                    <button type="submit" id="myBtn" class="btn btn-theme effect btn-sm" 
                                       
                                        style="color: #fff !important;">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                         Unfollow
                                    </button> 
                                  </form>

                              @else    

                                <form action="{{ route('user.follow', $post->user->id) }}" 
                                       method="POST">
                                   @csrf
                                    <button type="submit" id="myBtn"
                                     class="btn btn-theme effect btn-sm" 
                                       
                                        style="color: #fff !important;">
                                        <i class="fa fa-bookmark" aria-hidden="true"></i>
                                         Follow
                                    </button> 
                                  </form>

                              @endif
                            @endif        
                        @endguest

               </div>
    </div>

     <p>
	     <span class="opacity-70 r-1">Author</span>
	     <a href="{{ route('blog.writer', $post->user->id) }}" class="text-white"> 
	      {{ $post->user->fname }} {{ $post->user->lname }}
	     </a>
     </p>

     <p>

    @if(!empty($post->user->image))
     	<img class="avatar avatar-sm" 
     	     style="border-radius: 50%; height: 40px; width: 40px;" 
     	     src="{{ asset('data/'.$post->user->image) }}">
    @else
    <img class="avatar avatar-sm" 
           style="border-radius: 50%; height: 40px; width: 40px;" 
           src="{{ Gravatar::src($post->user) }}">
    @endif       


     </p>

  @if($post->likes()->where(['like' => '1'])->count() > 0)
     <div class="likeCount">
       <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
       {{ $post->likes()->where(['like' => '1'])->count() }} Likes
     </div>
  @endif

  
  @if($post->views > 0)
     <div class="likeCount">
       <span class="ti-eye"></span>
       {{ $post->views }} Views
     </div>
  @endif
     <p>
        <img src="{{ asset('data/'.$post->image) }}" alt="{{ $post->title }}" 
             style="width: 100%">
     </p>

     <p>
     	{!! $post->content !!}
     </p> 

<hr style="padding-bottom: 20px;">
       <!-- Start comment-sec Area -->
            <section class="comment-sec-area pt-80 pb-80">
              <div class="container">
                <div class="row flex-column">
                  <h5 class="text-uppercase pb-80">{{$post->comments->count()}} Comments</h5>
                  <br />
                @foreach ($post->comments as $comment)
                <div class="comment">
                      <div class="comment-list">
                        <div class="single-comment justify-content-between d-flex">
                          <div class="user justify-content-between d-flex">
                            <div class="thumb-comment">
                             @if(!empty($comment->user->image)) 
                              <img src="{{ asset('data/'.$comment->user->image) }}" alt="" width="50px">
                             @else
                             <img src="{{ Gravatar::src($comment->user->fname) }}" alt="" width="50px">
                             @endif 

                            </div>
                            <div class="desc">
                              <h5><a href="#">{{$comment->user->fname}} {{$comment->user->lname}}</a></h5>
                              <p class="date">{{$comment->created_at->format('D, d M Y H:i')}}</p>
                              <p class="comment">
                                {{$comment->message}}
                              </p>
                            </div>
                          </div>
                          <div class="">
                            <button class="btn-reply text-uppercase" id="reply-btn"
                              onclick="showReplyForm('{{$comment->id}}','{{$comment->user->fname}} {{$comment->user->lname}}')">reply</button
                            >
                          </div>
                        </div>
                      </div>
                    @if($comment->replies->count() > 0)
                      @foreach ($comment->replies as $reply)
                      <div class="comment-list left-padding">
                        <div
                          class="single-comment justify-content-between d-flex"
                        >
                          <div class="user justify-content-between d-flex">
                            <div class="thumb-comment">
                              <img src="{{ asset('data/'.$reply->user->image) }}" alt="{{$reply->user->fname}}" width="50px"/>
                            </div>
                            <div class="desc">
                              <h5><a href="#">{{$reply->user->fname}} {{$reply->user->lname}}</a></h5>
                              <p class="date">{{$reply->created_at->format('D, d M Y H:i')}}</p>
                              <p class="comment">
                                {{$reply->message}}
                              </p>
                            </div>
                          </div>
                          <div class="">
                            <button class="btn-reply text-uppercase" id="reply-btn"
                              onclick="showReplyForm('{{$comment->id}}','{{$reply->user->fname}} {{$reply->user->lname}}')">reply</button
                            >
                          </div>
                        </div>
                      </div>

                      @endforeach
                    @else
                    @endif
                      {{-- When user login show reply fourm --}}
                      @guest
                      {{-- Show none --}}
                      @else
                      <div class="comment-list left-padding" id="reply-form-{{$comment->id}}" style="display: none">
                        <div
                          class="single-comment justify-content-between d-flex"
                        >
                          <div class="user justify-content-between d-flex">
                            <div class="thumb-comment">
                              <img src="{{ Gravatar::src(Auth::user()->fname) }}" alt="{{Auth::user()->image}}" width="50px"/>
                            </div>
                            <div class="desc">
                              <h5><a href="#">{{Auth::user()->fname}} {{Auth::user()->lname}}</a></h5>
                              <p class="date">{{date('D, d M Y H:i')}}</p>
                              <div class="row flex-row d-flex">
                              <form action="{{route('reply.store',$comment->id)}}" method="POST">
                              @csrf
                                <div class="col-lg-12">
                                  <textarea
                                    id="reply-form-{{$comment->id}}-text"
                                    cols="60"
                                    rows="2"
                                    class="form-control mb-10"
                                    name="message"
                                    placeholder="Messege"
                                    onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Messege'"
                                    required
                                  ></textarea>
                                </div>
                                <button type="submit" class="btn-reply text-uppercase ml-3">Reply</button>
                              </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      @endguest
                  </div>
                 @endforeach
                </div>
              </div>
            </section>
            <!-- End comment-sec Area -->

            <!-- Start commentform Area -->
            <section class="commentform-area pb-120 pt-80 mb-100">
            @guest
                <div class="container">
                    <h4>Please Sign in to post comments - <a href="{{route('login')}}">Sing in</a> or <a href="{{route('register')}}">Register</a></h4>
                </div>
            @else
                <div class="container">
                  <h5 class="text-uppercas pb-50">Comment here</h5>
                  <div class="row flex-row d-flex">
                      <div class="col-lg-12">
                          <form action="{{route('comment.store', $post->id)}}" method="POST">
                              @csrf
                          <textarea
                            class="form-control mb-10"
                            name="message"
                            onfocus="this.placeholder = ''"
                            onblur="this.placeholder = 'Comment'"
                            required=""
                          ></textarea>
                          <button type="submit" class="primary-btn mt-20" href="#">Comment</button>
                      </form>
                      </div>
                  </div>
                </div>
                @endguest
            </section>
            <!-- End commentform Area -->



     @guest
      <div class="likewrap">
        <a href="{{ route('login') }}" class="liker">
            Like
        </a> 
        <a href="{{ route('login') }}" class="liker">
            Unlike
        </a> 
       </div> 
      @else
      <article  class="likewrap" data-postid="{{ $post->id }}">

        <a href="#" class="like">
         <!-- <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> -->
            {{  Auth::user()->likes()->where('post_id', $post->id)->first() ?
                Auth::user()->likes()->where('post_id', $post->id)->first()->like == 1 ? 
                'You like this post' : 'Like' : 'Like'  
            }}
         </a> 
        
        <a href="#" class="like">
          <!-- <i class="fa fa-thumbs-o-down" aria-hidden="true"></i> -->
             {{  Auth::user()->likes()->where('post_id', $post->id)->first() ? 
                 Auth::user()->likes()->where('post_id', $post->id)->first()->like == 0 ? 
                 'You don\'t like this post' : 'Dislike' : 'Dislike'  
             }}
        </a>

      </article >

      @endif

          <p>
            @foreach($post->tags as $tag)
              <a href="{{ route('blog.tag', $tag->id) }}" 
                  class="badge badge-pill badge-primary">
              	{{ $tag->name }}
              </a>
            @endforeach  
          </p>
          
         </div>
       </div>
    </div>
  </div>


<style type="text/css">
  .dropdown-item{
    display: block;
  }
  .dropdown-item:hover{
    color: #666;
  }
  body{
    font-size: 17px !important;
    line-height: 30px; color: #666;
    font-family: 'Poppins', sans-serif;
  }
 /* .dropdown-menu{
    padding: 15px !important;
  }*/
/*  body{
    font-size: 16px !important;
  }*/
  .likewrap {margin-bottom: 25px}
  .badge {
    background: #ffc41c;
    font-weight: normal;
    font-size: 14px;
    padding: 8px 10px;
    margin-right: 10px; margin-bottom: 10px;
  }
  .likeCount {width: 100px !important;}
</style>

@endsection

@section('scripts')

<script type="text/javascript">
  var token = '{{ Session::token() }}';
  var urlLike = '{{ route('like') }}';


function showReplyForm(commentId,user) {
      var x = document.getElementById(`reply-form-${commentId}`);
      var input = document.getElementById(`reply-form-${commentId}-text`);
      if (x.style.display === "none") {
        x.style.display = "block";
        input.innerText=`@${user} `;
      } else {
        x.style.display = "none";
      }
    }
  
</script>

<link rel="stylesheet" type="text/css" 
     href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

  <script type="text/javascript">              
  @if(session()->has('success'))
  toastr.options =
  {
    "closeButton" : true,
    "progressBar" : true
  }
      toastr.success("Commented successfully");
  @endif
</script>



@endsection

