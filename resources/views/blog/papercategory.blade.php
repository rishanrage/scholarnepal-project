@extends('layouts.main')

@section('title')
 Papers | Scholar Nepal :: Digital Journal 
@endsection


@section('content')


<div class="row" style="
                   /*background: #fff7ee; */
                   /*text-align: center; */ padding-left: 13%;
                   margin-bottom: 20px;">
  <div class="col-md-10" style="border-bottom: 1px solid #666; padding-left: 0%;">
  
  <div style="float: left; width: 65%">




     <h1 style="color: #666; font-size: 35px; 
    /*margin-left: 13.333%; */
     padding: 25px 0 5px 0; margin-bottom: 4px;">
             {{ $cat->name }}
     </h1>
 

    </div>

    

  </div>
</div>

 <div class="container">

    <div class="row">

      <div class="col-md-10 col-md-offset-1">
          
			
		<div class="event-items">
               
             @if($papers->count() > 0)   
             @foreach($papers as $l)

                <div class="item">
                          
                            <div class="col-md-8 info" style="padding-left: 0px;">
                                <div class="info-box">
                                    <div class="date">
                                        <!-- <strong>16</strong> Apr, 2020 -->
                                <i class="fa fa-calendar" aria-hidden="true" style="margin-right: 5px;"></i>
                                 {{ date('d-m-Y', strtotime($l->published_at)) }}

                            <!--     |  {{ \Carbon\Carbon::parse($l->published_at)->diffForHumans() }} -->

                     
                                    </div>
                                    <div class="content">
                                        <h4  style="margin-bottom:0px;">
                                            <a href="{{ route('paper.uploads', $l->id) }}" style="line-height:29px;">
                                               {{ $l->title }}
                                            </a>
                                        </h4>
                                      <!--   <ul>
                                            <li><i class="fas fa-clock"></i> 5 min to Read</li>
                                        </ul> -->
                                        <p style="font-size:17px;">
                                
                                        {{ str_limit($l->body,170) }}

                                        </p>
                                      <!--   <div class="bottom">
                                            <a href="#" class="btn circle btn-dark border btn-sm">
                                                <i class="fas fa-chart-bar"></i> Book Now
                                            </a>
                                            <a href="#">
                                                <i class="fas fa-ticket-alt"></i> 126 Available
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                            </div>

                </div>
                <div style="clear: both; height: 15px;"></div>
             @endforeach
             <div style="clear: both; height: 15px;"></div>
             @else
                <p style="padding: 100px">No Data at the moment..</p>
             @endif
                        
         </div>

				

        </div>
    </div>
</div>

<style type="text/css">
	.fa-clock, .fa-calendar{
		color: #ffc41c;
	}
	.date{
		margin-bottom: 5px;
	}
	.content h4 {
		margin-bottom: 10px;
	}
	.thumb img {
		width: 100%;
	}


  .dropdown-item{
    display: block;
  }
  .dropdown-item:hover{
    color: #666;
  }
/*  .dropdown-menu{
    padding: 15px !important;
  }*/
  .UL_A li{float: left; }
  .UL_A li a{color: #666666; padding:0 15px;}
/*  body{
    font-size: 16px !important;
  }*/
</style>



@endsection