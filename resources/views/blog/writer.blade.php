@extends('layouts.main')

@section('title')
 {{ $writerProfile->fname }} {{ $writerProfile->lname }} | Scholar Nepal :: Digital Journal
@endsection

@section('content')

<?php
    use Illuminate\Support\Str;
    Str::macro('readDuration', function(...$text) {
        $totalWords = str_word_count(implode(" ", $text));
        $minutesToRead = round($totalWords / 100);

        return (int)max(1, $minutesToRead);
    });
  ?>

<div class="row" style="
                   /*background: #fff7ee; */
                   /*text-align: center; */ padding-left: 13%;
                   margin-bottom: 40px;">
  <div class="col-md-10" style="border-bottom: 1px solid #949086; padding-left:0%;">
  
  <div style="float: left; width: 45%">

  @if(!empty($writerProfile->image))

  	 <img class="avatar avatar-sm" 
     	     style="float: left; margin-top: 28px; margin-bottom: 25px;  width: 75px; height: 75px; border-radius: 50%; border: 1px solid #f1f1f1; margin-right: 15px;" 
     	     src="{{ asset('data/'.$writerProfile->image) }}">

  @else

  <img class="avatar avatar-sm" 
           style="float: left; margin-top: 28px; margin-bottom: 25px;  width: 75px; height: 75px; border-radius: 50%;  margin-right: 15px;" 
           src="{{ Gravatar::src($writerProfile->fname) }}">

 @endif


     <h1 style="color: #837e71; font-size: 20px;
    /*margin-left: 13.333%; */
     padding: 25px 0 0px 0; margin-bottom: 4px;">{{ $writerProfile->fname }} {{ $writerProfile->lname }}</h1>
     <p style="margin-bottom: 0px;">
       Joined : {{ date('d-m-Y', strtotime($writerProfile->created_at)) }} 
     </p>
      <p>Follower : {{ $followers->count() }} </p>
    </div>

    <div style="float: left; width: 30%; margin-top: 63px;">
      <ul class="UL_A">
         <li><a href="#" title="About">About</a>/</li>
         <li>
          <a href="{{ route('paper', $writerProfile->id) }}" title="Papers">Papers</a>/</li>
         <li>
          <a href="#" title="share">
            <i class="fa fa-share-alt" aria-hidden="true"></i>
          </a>
        </li>
         <!-- <li><a href="#">About</a></li> -->
      </ul>
    </div>

     <div class="align-right" style="margin-top: 50px;">
                    @guest
                        <a href="{{ route('login') }}"
                          class="btn btn-theme effect btn-sm" 
                          style="color: #fff !important;">
                          <i class="fa fa-bookmark" aria-hidden="true"></i>
                            Follow
                        </a> 
                        
                      @else

                          @if(Auth::user()->id != $writerProfile->id)
                            @php
                                $user = App\User::findOrFail(Auth::user()->id);
                                $checkUser = App\User::findOrFail($writerProfile->id);
                            @endphp

                              @if($user->isFollowing($checkUser)) 

                                 <form action="{{ route('user.unfollow', $writerProfile->id) }}" 
                                       method="POST">
                                   @csrf
                                    <button type="submit" id="myBtn" class="btn btn-theme effect btn-sm" 
                                       
                                        style="color: #fff !important;">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                         Unfollow
                                    </button> 
                                  </form>

                              @else    

                                <form action="{{ route('user.follow', $writerProfile->id) }}" 
                                       method="POST">
                                   @csrf
                                    <button type="submit" id="myBtn"
                                     class="btn btn-theme effect btn-sm" 
                                       
                                        style="color: #fff !important;">
                                        <i class="fa fa-bookmark" aria-hidden="true"></i>
                                         Follow
                                    </button> 
                                  </form>

                              @endif
                            @endif        
                        @endguest

               </div>

  </div>
</div>

 <div class="container">

    <div class="row">

      <div class="col-md-10 col-md-offset-1">
          
			
		<div class="event-items">
               
             @if($writerPost->count() > 0)   
             @foreach($writerPost as $l)


                <div class="item">
                            <div class="col-md-10 thumb" 
                                 style="padding-left: 0px; margin-bottom: 10px;">
                            	<img src="{{ asset('data/'.$l->image) }}">
                            </div>
                            <div class="col-md-7 info" style="padding-left: 0px;">
                                <div class="info-box">
                                    <div class="date">
                                        <!-- <strong>16</strong> Apr, 2020 -->
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                {{ date('d-m-Y', strtotime($l->published_at)) }}

                                |  {{ \Carbon\Carbon::parse($l->published_at)->diffForHumans() }}

                                |  <i class="fas fa-clock"></i> 
                                {{Str::readDuration($l->content). ' min to read'}}

                                    </div>
                                    <div class="content">
                                        <h4>
                                            <a href="{{ route('blog.show', $l->id) }}" style="line-height: 30px;">
                                               {{ $l->title }}
                                            </a>
                                        </h4>
                                      <!--   <ul>
                                            <li><i class="fas fa-clock"></i> 5 min to Read</li>
                                        </ul> -->
                                        <p style="font-size: 17px;">
                                
                                        {{ str_limit($l->description,170) }}

                                        </p>
                                 
                                    </div>
                                </div>
                            </div>

                </div>
                <div style="clear: both; height: 35px;"></div>
             @endforeach
             @else
                <p style="padding: 100px">No Data at the moment..</p>
             @endif
                        
         </div>

				

        </div>
    </div>
</div>

<style type="text/css">
	.fa-clock, .fa-calendar{
		color: #ffc41c;
	}
	.date{
		margin-bottom: 5px;
	}
	.content h4 {
		margin-bottom: 0px;
	}
	.thumb img {
		width: 100%;
	}


  .dropdown-item{
    display: block;
  }
  .dropdown-item:hover{
    color: #666;
  }
/*  .dropdown-menu{
    padding: 15px !important;
  }*/
  .UL_A li{float: left; }
  .UL_A li a{color: #666666; padding:0 15px;}
/*  body{
    font-size: 16px !important;
  }*/
</style>



@endsection