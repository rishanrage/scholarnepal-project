@extends('layouts.app')

@section('title')
Soholar Nepal | Dashboard
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                  <!--   @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif     -->   

                    <div class="row">

                    @if($followings->count() > 0)
                     
                      <div class="col-sm-6">
                        <div class="card">
                          <div class="card-body">
                            <h5 class="card-title">Following <span class="badge badge-warning"> 
                            {{ $followings->count() }}</span></h5>
                            @foreach($followings as $following )
                             <a href="{{ route('blog.writer', $following->id) }}" class="card-text" style="display: block; margin: 0 0 10px 0">

                            @if(!empty($following->image))
                             <img style=" margin-right: 10px; width: 30px; height: 30px; border-radius: 50%" src="{{ asset('data/'.$following->image) }}" alt="{{ $following->image }}"/>
                            @else
                             <img style=" margin-right: 10px; width: 30px; height: 30px; border-radius: 50%" src="{{ Gravatar::src($following->fname) }}" alt="{{ $following->image }}"/>
                             @endif

                                {{ $following->fname }}  {{ $following->lname }}
                             </a>
                            @endforeach
                          </div>
                        </div>
                      </div>
                    
                    @endif 

                    @if($followers->count() > 0)
                     
                      <div class="col-sm-6">
                        <div class="card">
                          <div class="card-body">
                            <h5 class="card-title">follower <span class="badge badge-warning"> 
                            {{ $followers->count() }}</span></h5>
                            @foreach($followers as $follower )
                             <a href="{{ route('blog.writer', $follower->id) }}" class="card-text" style="display: block; margin: 0 0 10px 0">

                             @if(!empty($follower->image))
                             <img style=" margin-right: 10px; width: 30px; height: 30px; border-radius: 50%" src="{{ asset('data/'.$follower->image) }}" alt="{{ $follower->image }}"/>
                            @else
                             <img style=" margin-right: 10px; width: 30px; height: 30px; border-radius: 50%" src="{{ Gravatar::src($follower->fname) }}" alt="{{ $follower->image }}"/>
                             @endif

                             
                                {{ $follower->fname }}  {{ $follower->lname }}
                             </a>
                            @endforeach
                          </div>
                        </div>
                      </div>
                    
                    @endif 


                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
