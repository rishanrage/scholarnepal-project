<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login | Website Administration</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="{{ asset('admin/bootstrap.min.css') }}">

<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/login/main.css') }}">
</head>
<body>
 
	<div class="limiter">
		<div class="container-login100" 
		     style="background-image: url('{{ asset('admin/login/login.jpg') }}';">
			<div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">

			@if(session('status'))
		        <div class="alert alert-success alert-dismissible fade show" role="alert" style="text-align: center; margin-bottom: 10px;">
		            {{session('status')}}
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                <span aria-hidden="true">&times;</span>
		            </button>
		        </div>
		    @endif

		    @if(session('error'))
		        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="text-align: center; margin-bottom: 10px;">
		            {{session('error')}}
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                <span aria-hidden="true">&times;</span>
		            </button>
		        </div>
		    @endif

				<form method="POST" action="{{ route('admin.login') }}" 
				      class="login100-form validate-form flex-sb flex-w">
				@csrf
					<span class="login100-form-title p-b-53">
						Website Administration
					</span>

					<div class="p-t-31 p-b-9">
						<span class="txt1">
							Username
						</span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Username is required">
						<input class="input100{{ $errors->has('email') ? ' is-invalid' : '' }}" type="text" id="email" name="email" required>				        
					</div>

					<div class="p-t-13 p-b-9">
						<span class="txt1">
							Password
						</span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" id="password" required>					
					</div>
                           @if ($errors->has('email'))
				               <span style="font-size: 12px;
										margin-bottom: 15px;
										display: block;
										text-align: center;">
                                        <strong>{{ $errors->first('email') }}</strong>
                                 </span>      
                             @endif

                           @if ($errors->has('password'))
				              <span style="font-size: 12px;
										margin-bottom: 15px;
										display: block;
										text-align: center;">
                                        <strong>{{ $errors->first('password') }}</strong>
                                 </span>      
                             @endif       
					<div class="container-login100-form-btn m-t-17">
						<button type="submit" class="login100-form-btn">
							Sign In
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>	

</body>
</html>
