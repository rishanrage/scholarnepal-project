<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('admin/assets/vendors/mdi/css/materialdesignicons.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('admin/assets/vendors/flag-icon-css/css/flag-icon.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('admin/assets/vendors/css/vendor.bundle.base.css') }}" />
    <link rel="stylesheet" href="{{ asset('admin/assets/vendors/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('admin/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('admin/assets/css/style.css') }}" />
    <link rel="shortcut icon" href="assets/images/index.png" />
  </head>
  <body>
    <div class="container-scroller">
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <div class="text-center sidebar-brand-wrapper d-flex align-items-center">
          <a class="sidebar-brand brand-logo" href="#">
              <img src="{{ asset('admin/assets/images/logo-2.png') }}" alt="logo" />
          </a>
          <a class="sidebar-brand brand-logo-mini pl-4 pt-3" href="#">
             <img src="{{ asset('admin/assets/images/ico.png') }}" alt="logo" />
          </a>
        </div>

        <ul class="nav">

         

          <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.home') }}">
              <i class="mdi mdi-home menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          
          
          <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.users') }}">
              <i class="mdi mdi-account-multiple"></i>
              <span class="menu-title">Users Management</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.posts') }}">
              <i class="mdi mdi-paperclip"></i>
              <span class="menu-title">Post Management</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.categories') }}">
              <i class=" mdi mdi-format-list-bulleted"></i>
              <span class="menu-title">Categories Management</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.tags') }}">
              <i class="mdi mdi-pound"></i>
              <span class="menu-title">Tags Management</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.pages') }}">
              <i class=" mdi mdi-note-plus"></i>
              <span class="menu-title">Page Management</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.news') }}">
              <i class=" mdi mdi-newspaper"></i>
              <span class="menu-title">News Management</span>
            </a>
          </li>
          
          <li class="nav-item sidebar-actions">
            <div class="nav-link">
              <div class="mt-4">
                <ul class="mt-4 pl-0">
                  <li>
                  <a href="{{ route('admin.logout') }}">Sign Out</a></li>
                </ul>
              </div>
            </div>
          </li>
        </ul>
      </nav>



      <div class="container-fluid page-body-wrapper">
        <nav class="navbar col-lg-12 col-12 p-lg-0 fixed-top d-flex flex-row">

          <div class="navbar-menu-wrapper 
                     d-flex align-items-stretch 
                    justify-content-between">

            <button class="navbar-toggler navbar-toggler align-self-center mr-2" 
                    type="button" data-toggle="minimize">
              <i class="mdi mdi-menu"></i>
            </button>

            <ul class="navbar-nav">                         
              <li class="nav-item nav-search border-0 
                         ml-1 ml-md-3 ml-lg-5 d-none d-md-flex">
                <form class="nav-link form-inline mt-2 mt-md-0">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-magnify"></i>
                      </span>
                    </div>
                  </div>
                </form>
              </li>
            </ul>


            <ul class="navbar-nav navbar-nav-right ml-lg-auto">
              
              <li class="nav-item nav-profile dropdown border-0">
                <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown">
                  <img class="nav-profile-img mr-2" alt="" src="{{ Gravatar::src(Auth::user()->lname) }}" />
                  <span class="profile-name">{{ Auth::user()->email }}</span>
                </a>
                <div class="dropdown-menu navbar-dropdown w-100" 
                     aria-labelledby="profileDropdown">
                  <a class="dropdown-item" href="{{ route('admin.logout') }}">
                    <i class="mdi mdi-logout mr-2 text-primary"></i> Signout </a>
                </div>
              </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" 
                    type="button" data-toggle="offcanvas">
              <span class="mdi mdi-menu"></span>
            </button>
          </div>
        </nav>


        <div class="main-panel">
          <div class="content-wrapper pb-0">
            <div class="container">
                @if(session()->has('success'))
                  <div class="alert alert-success">
                   {{ session()->get('success') }}
                  </div>
                @endif
             </div>

              <div class="container">
                @if($errors->any())

                  <div class="alert alert-danger">
                    <ul class="list-group">
                      @foreach($errors->all() as $error)
                       <li class="list-group-item text-danger">{{ $error }}</li>
                      @endforeach 
                    </ul>
                  </div>

                @endif
             </div> 
            
            
            @yield('content')
           
          </div>
          <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bitsnp.com 2021</span>
          <!--     <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="#" target="_blank">
                  Bootstrap dashboard template</a> from Bootstrapdash.com
              </span> -->
            </div>
          </footer>
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{ asset('admin/assets/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('admin/assets/vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('admin/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('admin/assets/vendors/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('admin/assets/vendors/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('admin/assets/vendors/flot/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('admin/assets/vendors/flot/jquery.flot.fillbetween.js') }}"></script>
    <script src="{{ asset('admin/assets/vendors/flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('admin/assets/vendors/flot/jquery.flot.pie.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('admin/assets/js/off-canvas.js') }}"></script>
    <script src="{{ asset('admin/assets/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('admin/assets/js/misc.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{ asset('admin/assets/js/dashboard.js') }}"></script>
    @yield('scripts')
    <!-- End custom js for this page -->
  </body>
</html>