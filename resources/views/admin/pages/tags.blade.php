@extends('admin.layouts.app')

@section('title')
Tag Management | Website Administration
@endsection  


@section('content')
<div class="row">
             
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">
                      Tag Management  
                       <label class="badge badge-warning">
                         {{ $tags->count() }}
                       </label>
                    </h4>
           
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>Publish Date</th>
                            <th>Name</th>                
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($tags as $tag)
                          <tr>
                            <td>
                             {{ date('d-m-Y', strtotime($tag->created_at)) }}
                            </td>
                            <td>{{ $tag->name }}</td>
                            <td>
                              <input data-id="{{$tag->id}}" class="toggle-class badge badge-success" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="Inative" {{ $tag->status ? 'checked' : '' }}>
                            </td>

                          </tr>
                        @endforeach  
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

         
            
       
            </div>
@endsection

@section('scripts')

<script>

  $(function() {
    $('.toggle-class').change(function() {
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var id = $(this).data('id'); 

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/statusTag',
            data: {'status': status, 'id': id},
            success: function(data){
             console.log(data.success)
            }
        });
    })
  })



</script>
 <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@endsection