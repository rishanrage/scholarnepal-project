@extends('admin.layouts.app')

@section('title')
 User Management | Website Administration
@endsection  


@section('content')
<div class="row">
             
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">
                      Users Management  
                       <label class="badge badge-warning">
                         {{ $users->count() }}
                       </label>
                    </h4>

                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Image</th> 
                            <th>Email</th>
                            <th>Type</th>
                            <th>Status</th>                    
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                          <tr>
                            <td> {{ date('d-m-Y', strtotime($user->created_at)) }}</td>
                            <td>{{ $user->fname }} {{ $user->lname }}</td>
                            <td>
                            @if(!empty($user->image))
                                <img class="nav-profile-img mr-2" alt="" src="{{ asset('data/'.$user->image) }}" />
                            @else
                              <img class="nav-profile-img mr-2" alt="" src="{{ Gravatar::src($user->lname) }}" />  
                            @endif    

                            </td>
                            <td>{{ $user->email }}</td>

                            <td>
                                  @if($user->type == 1)
                                      Individual User
                                  @elseif($user->type == 2) 
                                       Academic Institute
                                  @elseif($user->type == 3) 
                                        Organisation  
                                  @endif   

                               </td>                            
                            <td>
                              <input data-id="{{$user->id}}" class="toggle-class badge badge-success" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="Inative" {{ $user->status ? 'checked' : '' }}>
                            </td>
                            <td>
                              <a href="#" class="badge badge-primary">
                                 View Detail
                              </a>
                            </td>
                          </tr>
                        @endforeach  
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

         
            
       
            </div>
@endsection


@section('scripts')

<script>

  $(function() {
    $('.toggle-class').change(function() {
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var id = $(this).data('id'); 

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/statusUsers',
            data: {'status': status, 'id': id},
            success: function(data){
             console.log(data.success)
            }
        });
    })
  })

</script>
 <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@endsection