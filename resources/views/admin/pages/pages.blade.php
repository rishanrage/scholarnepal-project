@extends('admin.layouts.app')

@section('title')
Page Management | Website Administration
@endsection  


@section('content')
<div class="row">
             
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title" 
                        style="width: 30%;
                                float: left;
                                margin-bottom: 35px;">
                      Page Management  
                    </h4>
                    <a href="{{ route('admin.create') }}" 
                       class="btn btn-warning" style="float: right;"> 
                      Add Page 
                    </a>

                  <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>Date</th>
                            <th>Title</th>                
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                      
                          <tr>
                            <td>
                               1212
                            </td>
                            <td>sdf</td>
                            <td>
                              <input data-id="{{$tag->id}}" class="toggle-class badge badge-success" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="Inative" {{ $tag->status ? 'checked' : '' }}>
                            </td>

                          </tr>
             
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

         
            
       
            </div>
@endsection

@section('scripts')

<script>

  $(function() {
    $('.toggle-class').change(function() {
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var id = $(this).data('id'); 

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/statusPage',
            data: {'status': status, 'id': id},
            success: function(data){
             console.log(data.success)
            }
        });
    })
  })



</script>
 <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@endsection