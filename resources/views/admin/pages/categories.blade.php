@extends('admin.layouts.app')

@section('title')
Category Management | Website Administration
@endsection  


@section('content')
<div class="row">
             
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">
                      Category Management  
                       <label class="badge badge-warning">
                         {{ $categories->count() }}
                       </label>
                    </h4>
              
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>Publish Date</th>
                            <th>Name</th>
                            <th>Icon</th>
                            <th>Featured</th>               
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                          <tr>
                            <td>
                             {{ date('d-m-Y', strtotime($category->created_at)) }}
                            </td>
                            <td>{{ $category->name }}</td>
                            <td>
                                <form  method="post" enctype="multipart/form-data" id="imageUpload">
                                    <input type="file" name="icon"/>
                                    <button type="submit" id="icon">Save</button>
                                </form>
                            </td>

                            <td>
                             
                                 <input data-id="{{$category->id}}" class="toggle-class badge badge-success" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Featured" data-off="Not Featured" {{ $category->featured ? 'checked' : '' }}>
                            
                            </td>

                            <td>
                              <input data-id="{{$category->id}}" class="toggle-class1 badge badge-success" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="Inative" {{ $category->status ? 'checked' : '' }}>
                            </td>

                          </tr>
                        @endforeach  
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

                
            </div>

@endsection

@section('scripts')

<script>

  $(function() {
    $('.toggle-class').change(function() {
        var featured = $(this).prop('checked') == true ? 1 : 0; 
        var id = $(this).data('id'); 

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/featuredCategories',
            data: {'featured': featured, 'id': id},
            success: function(data){
             console.log(data.success)
            }
        });
    })
  })


  $(function() {
    $('.toggle-class1').change(function() {
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var id = $(this).data('id'); 

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/statusCategories',
            data: {'status': status, 'id': id},
            success: function(data){
             console.log(data.success)
            }
        });
    })
  })

</script>


<script type="text/javascript">

        $(document).ready(function () {

            $('.success').hide();// or fade, css display however you'd like.

        });


        $('#imageUpload').on('submit',(function(e) {

            $.ajaxSetup({

                headers: {

                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });

             

            e.preventDefault();

            var formData = new FormData(this);

            $.ajax({

               type:'POST',

               url: "{{ route('Imagestore')}}",

               data:formData,

               cache:false,

               contentType: false,

               processData: false,

             

                 complete: function(response) 

                {

                    if($.isEmptyObject(response.responseJSON.error)){

                            $('.success').show();

                           setTimeout(function(){

                           $('.success').hide();

                        }, 5000);

                    }else{

                        printErrorMsg(response.responseJSON.error);

                    }

                }


            });

        }));

       function printErrorMsg(msg){

               $(".print-error-msg").find("ul").html('');

            $(".print-error-msg").css('display','block');

            $.each( msg, function( key, value ) {

                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

            });

       }

    </script>


 <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@endsection