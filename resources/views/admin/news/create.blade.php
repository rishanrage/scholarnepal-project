@extends('admin.layouts.app')

@section('title')
Create News | Website Administration
@endsection  


@section('content')
<div class="row">
             
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Create News</h4>
                    <hr style="margin-bottom: 
                    30px;">  


                    <form class="forms-sample" action="{{ isset($news) ? route('news.update', $news->id) : route('news.store') }}" method="POST" enctype="multipart/form-data">
                     @csrf
                       @if(isset($news))
                         @method('PUT')
                       @endif
                     
                      <div class="form-group">
                        <label for="Title">Title</label>
                        <input type="text" class="form-control" id="Title" placeholder="Title" name="title" value="{{ isset($news) ? $news->title : '' }}">
                      </div>

                      <div class="form-group">
                        <label for="Description">Description</label>
                        <textarea class="form-control" id="Description" rows="4" name="description">
                          {{ isset($news) ? $news->description : '' }}
                        </textarea>
                      </div>

                    @if(isset($news))
                         <div class="form-group">
                           <img src="{{ asset('data/'.$news->image) }}" alt="" width="60" />
                         </div>
                      @endif

                      <div class="form-group">
                        <label>Image upload</label>
                        <input type="file" name="image" id="image" class="form-control">                       
                      </div>
                      <div class="form-group">
                        <label for="content">Content</label>
                        <textarea class="form-control" id="editor" rows="4" name="content">
                          {{ isset($news) ? $news->content : '' }}
                        </textarea>
                      </div>
                      <button type="submit" class="btn btn-primary mr-2"> 
                       {{ isset($news) ? 'Update News ': 'Create News' }}
                       </button>
                      <!-- <button class="btn btn-light">Cancel</button> -->
                    </form>
                  </div>
                </div>
              </div>

         
            
       
            </div>
@endsection

@section('scripts')
<script src="https://cdn.ckeditor.com/ckeditor5/27.1.0/classic/ckeditor.js"></script>


                <script>
                        ClassicEditor
                                .create( document.querySelector( '#editor' ) )

                                .then( editor => {
                                        console.log( editor );
                                } )
                                .catch( error => {
                                        console.error( error );
                                } );
                </script>
@endsection