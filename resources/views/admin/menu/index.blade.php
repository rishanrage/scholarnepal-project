@extends('admin.layouts.app')

@section('title')
Page Management | Website Administration
@endsection  


@section('content')
<div class="row">
             
             
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title" 
                        style="width: 30%;
                                float: left;
                                margin-bottom: 35px;">
                      Page Management <span class="badge badge-warning text-white ml-3 rounded">{{ $pages->count() }}</span>
                    </h4>
                    <a href="{{ route('admin.page.create') }}" 
                       class="btn btn-warning" style="float: right;"> 
                      Add Page 
                    </a>

                  <div class="table-responsive">
                  @if($pages->count() > 0)
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>Date</th>
                            <th>Title</th>
                            <th>Image</th> 
                             <th>Order</th>                  
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                      @foreach($pages as $page)
                          <tr>
                            <td>
                               {{ date('d-m-Y', strtotime($page->created_at)) }}
                            </td>
                            <td>{{ str_limit($page->title,70) }}</td>
                             <td>
                              <img src="{{ asset('data/'.$page->image) }}" width="60" height="60" />
                             </td>
                             <td>{{ $page->ordering }}</td>
                            <td>
                              <input data-id="{{$page->id}}" class="toggle-class badge badge-success" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="Inative" {{ $page->status ? 'checked' : '' }}>
                            </td>
                            <td>
                              <a href="{{ route('pages.edit', $page->id) }}"
                                 class="badge badge-success">
                                 Edit
                              </a>

                            <!--   <a href="#" class="badge badge-danger">
                                 Delete
                              </a> -->

                             <button class="badge badge-danger" onclick="handleDelete({{ $page->id }})">Delete</button>

                            </td>

                          </tr>
                       @endforeach

             
                        </tbody>
                      </table>
                       @else
                         <h6 class="text-center">No page created yet</h6>
                        @endif  

                        <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog">

        <form action="" method="POST" id="deletePage">
           @method('DELETE')
           @csrf          
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Delete Page</h5>
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">
                <p class="text-center">Are you sure you want to delete this Page?</p>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No, Go back</button>
                <button type="submit" class="btn btn-danger">Yes, Delete</button>
              </div>
            </div>
          </form>

      </div>
    </div>

                    </div>
                  </div>
                </div>
              </div>

         
            
       
            </div>
@endsection

@section('scripts')

<script type="text/javascript">
  
  function handleDelete(id) {    
    var form = document.getElementById('deletePage')
    form.action = '/pages/' + id
    console.log('deleting.', form);
    $('#deleteModal').modal('show')
  }


  $(function() {
    $('.toggle-class').change(function() {
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var id = $(this).data('id'); 

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/statusPage',
            data: {'status': status, 'id': id},
            success: function(data){
             console.log(data.success)
            }
        });
    })
  })


</script>

 <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

@endsection