@extends('admin.layouts.app')

@section('title')
Create Page | Website Administration
@endsection  


@section('content')
<div class="row">
             
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Create Page</h4>
                    <hr style="margin-bottom: 
                    30px;">  


                    <form class="forms-sample" action="{{ isset($page) ? route('pages.update', $page->id) : route('pages.store') }}" method="POST" enctype="multipart/form-data">
                     @csrf
                       @if(isset($page))
                         @method('PUT')
                       @endif
                     
                      <div class="form-group">
                        <label for="Title">Title</label>
                        <input type="text" class="form-control" id="Title" placeholder="Title" name="title" value="{{ isset($page) ? $page->title : '' }}">
                      </div>

                      <div class="form-group">
                        <label for="Description">Description</label>
                        <textarea class="form-control" id="Description" rows="4" name="description">
                          {{ isset($page) ? $page->description : '' }}
                        </textarea>
                      </div>

                    @if(isset($page))
                         <div class="form-group">
                           <img src="{{ asset('data/'.$page->image) }}" alt="" width="60" />
                         </div>
                      @endif

                      <div class="form-group">
                        <label>Image upload</label>
                        <input type="file" name="image" id="image" class="form-control">                       
                      </div>
                      <div class="form-group">
                        <label for="content">Content</label>
                        <textarea class="form-control" id="editor" rows="4" name="content">
                          {{ isset($page) ? $page->content : '' }}
                        </textarea>
                      </div>

                      <div class="form-group">
                        <label for="Order">Order</label>
                        <input type="text" class="form-control" id="ordering" placeholder="Order" name="ordering" value="{{ isset($page) ? $page->ordering : '' }}" required>
                      </div>


                      <button type="submit" class="btn btn-primary mr-2"> 
                       {{ isset($page) ? 'Update Page ': 'Create Page' }}
                       </button>
                      <!-- <button class="btn btn-light">Cancel</button> -->
                    </form>
                  </div>
                </div>
              </div>

         
            
       
            </div>
@endsection

@section('scripts')
<script src="https://cdn.ckeditor.com/ckeditor5/27.1.0/classic/ckeditor.js"></script>


                <script>
                        ClassicEditor
                                .create( document.querySelector( '#editor' ) )

                                .then( editor => {
                                        console.log( editor );
                                } )
                                .catch( error => {
                                        console.error( error );
                                } );
                </script>
@endsection