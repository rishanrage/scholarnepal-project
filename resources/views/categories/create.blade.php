@extends('layouts.app')

@section('title')
  Scholar Nepal | Create Category
@endsection

@section('content')

<div class="card card-default">
  <div class="card-header">
    {{ isset($category) ? 'Edit Category' : 'Create Category' }}
  </div>

  <div class="card-body">
    
   @include('partials.errors')
  
    <form action="{{ isset($category) ? route('categories.update', $category->id) : route('categories.store') }}" method="POST">   
      @csrf

	     @if(isset($category))
	       @method('PUT')
	     @endif

      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" class="form-control" value="{{ isset($category) ? $category->name : ''}}">
      </div>

      <!--<div class="form-group">-->
      <!--  <label for="Icon">Icon Code</label>-->
      <!--  <input type="text" name="icon" id="icon" class="form-control" value="{{ isset($category) ? $category->icon : ''}}">-->
      <!--</div>-->

      <div class="form-group">
         <button class="btn btn-success">
           {{ isset($category) ? 'Update Category' : 'Add Category' }}
         </button>
      </div>

    </form>
  </div> 

</div>

@endsection