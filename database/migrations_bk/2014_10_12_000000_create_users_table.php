<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('lname');
            $table->string('email')->unique();
            $table->enum('role', ['writer','admin'])->default('writer');
            $table->integer('type');
            $table->string('location')->nullable();
            $table->string('gender')->nullable();
            $table->string('dob')->nullable();
            $table->string('c_profession')->nullable();
            $table->string('work_place')->nullable();
            $table->string('a_degree')->nullable();
            $table->string('a_institution')->nullable();
            $table->string('r_areas')->nullable();
            $table->string('name_institution')->nullable();
            $table->string('c_person')->nullable();
            $table->string('phone')->nullable();
            $table->string('t_organisation')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
