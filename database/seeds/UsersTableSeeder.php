<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'admin@admin.com')->first();
        if(!$user) {
        	User::create([
        		'fname' => 'Sunil',
        		'lname' => 'Thapa',
        		'email' => 'admin@admin.com',
        		'type' => '1',
        		'password' => Hash::make('123456')
        	]);
        }
    }
}
