<?php
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       
        $userA = User::where('email', 'tom@gmail.com')->first();
        if(!$userA) {
        	User::create([
        		'fname' => 'Tom',
        		'lname' => 'Cat',
        		'email' => 'tom@gmail.com',
        		'type' => '1',
        		'password' => Hash::make('password'),
        	]);
        }

        $userB = User::where('email', 'lord@gmail.com')->first();
        if(!$userB) {
        	User::create([
        		'fname' => 'Apache',
        		'lname' => 'Server',
        		'email' => 'lord@gmail.com',
        		'type' => '1',
        		'password' => Hash::make('password'),
        	]);
        }

        $userC = User::where('email', 'ram@gmail.com')->first();
        if(!$userB) {
            User::create([
                'fname' => 'Ram',
                'lname' => 'Pandit',
                'email' => 'ram@gmail.com',
                'type' => '1',
                'password' => Hash::make('password'),
            ]);
        }

        $userD = User::where('email', 'hari@gmail.com')->first();
        if(!$userB) {
            User::create([
                'fname' => 'Hari',
                'lname' => 'Pandit',
                'email' => 'hari@gmail.com',
                'type' => '1',
                'password' => Hash::make('password'),
            ]);
        }

        $userE = User::where('email', 'gopal@gmail.com')->first();
        if(!$userB) {
            User::create([
                'fname' => 'Gopal',
                'lname' => 'Singh',
                'email' => 'gopal@gmail.com',
                'type' => '1',
                'password' => Hash::make('password'),
            ]);
        }

        // $userA->followers()->attach(User::factory(10)->create()->pluck('id'));
        // $userA->following()->attach(User::factory(10)->create()->pluck('id'));
        

        // $userB->followers()->attach(User::factory(10)->create()->pluck('id'));
        // $userB->following()->attach(User::factory(10)->create()->pluck('id'));
        



        // $userA->following()->attach(factory(App\User::class, 50)->create()->pluck('id'));
        // $userA->followers()->attach(factory(App\User::class, 50)->create()->pluck('id'));


        // $userB->following()->attach(factory(App\User::class, 50)->create()->pluck('id'));
        // $userB->followers()->attach(factory(App\User::class, 50)->create()->pluck('id'));


    }
}


