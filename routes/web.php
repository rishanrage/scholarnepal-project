<?php

use App\Http\Controllers\Blog\PostsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');
Route::get('/blog/post/{post}', [PostsController::class, 'show'])->name('blog.show');
Route::get('/blog/category/{category}', [PostsController::class, 'category'])->name('blog.list');

// this route for pivot table for getting the posts list by tags
Route::get('/blog/tags/{tag}', [PostsController::class, 'tag'])->name('blog.tag');

// Route::get('/blog/writer/{post}', [PostsController::class, 'writer'])->name('blog.writer');

Route::get('/blog/contributor/{post}', [PostsController::class, 'writer'])->name('blog.writer');

Route::get('/paper/{post}', [PostsController::class, 'paper'])->name('paper');

Route::get('/paper/uploads/{paper}', [PostsController::class, 'uploads'])->name('paper.uploads');

Route::get('/paper/uploads/category/{category}', [PostsController::class, 'paperByCid'])->name('paper.category');

Auth::routes();

Route::middleware(['auth'])->group(function(){

	Route::get('/home', 'HomeController@index')->name('home');
	
	Route::resource('categories', 'CategoriesController');
	Route::resource('tags', 'TagsController');
	Route::resource('posts', 'PostsController');
	
	Route::get('trashed-posts', 'PostsController@trashed')->name('trashed-posts.index');
	Route::put('restore-post/{post}', 'PostsController@restore')->name('restore-posts');

	Route::get('users/profile', 'UsersController@edit')->name('users.edit-profile');
	Route::put('users/profile', 'UsersController@update')->name('users.update-profile');

    Route::get('/papers',  'PapersController@index')->name('papers.index');
	Route::get('/papers/upload', 'PapersController@create')->name('papers.upload');
	Route::post('/papers/store',  'PapersController@store')->name('papers.store');
	Route::get('papers/{paper}/edit', 'PapersController@edit')->name('papers.edit');
	Route::delete('papers/{paper}',  'PapersController@destroy');

	Route::post('profile/{profileId}/follow', 'ProfileController@followUser')->name('user.follow');
    Route::post('/{profileId}/unfollow', 'ProfileController@unFollowUser')->name('user.unfollow');

    Route::post('/like', [PostsController::class, 'likePost'])->name('like');

    Route::post('/comment/{post}', 'CommentController@store')->name('comment.store');
    Route::post('/comment-reply/{comment}', 'CommentReplyController@store')->name('reply.store');

    Route::get('/comments', 'CommentController@index')->name('comment.index');
    Route::delete('/comment/{id}', 'CommentController@destroy')->name('comment.destroy');
    Route::get('/reply-comments', 'CommentReplyController@index')->name('reply-comment.index');
    Route::delete('/reply-comment/{id}', 'CommentReplyController@destroy')->name('reply-comment.destroy');

    // Route::get('/post-liked-users/{post}', 'PostsController@likedUsers')->name('post.like.users');

});


Route::middleware(['auth', 'admin'])->group(function(){
	// Route::get('users/profile', 'UsersController@edit')->name('users.edit-profile');
	// Route::put('users/profile', 'UsersController@update')->name('users.update-profile');
	Route::get('users', 'UsersController@index')->name('users.index');
	Route::post('users/{user}/make-admin', 'UsersController@makeAdmin')->name('users.make-admin');

    Route::get('admin/dashboard', 'Admin\AdminController@dashboard')->name('admin.home');

    Route::get('admin/users', 'Admin\AdminController@users')->name('admin.users');

    Route::get('admin/posts', 'Admin\AdminController@posts')->name('admin.posts');

    Route::get('admin/categories', 'Admin\AdminController@categories')->name('admin.categories');

    Route::get('admin/tags', 'Admin\AdminController@tags')->name('admin.tags');

    Route::get('admin/pages', 'PagesController@index')->name('admin.pages');

    Route::get('admin/page/create', 'PagesController@create')->name('admin.page.create');

    Route::resource('pages', 'PagesController');


    Route::get('admin/news', 'NewsController@index')->name('admin.news');

    Route::get('admin/news/create', 'NewsController@create')->name('admin.create');

    Route::resource('news', 'NewsController');

    Route::get('featuredCategories', 'Admin\AdminController@featuredCategories');
    Route::get('statusCategories', 'Admin\AdminController@statusCategories');
    Route::get('statusUsers', 'Admin\AdminController@statusUsers');
    Route::get('statusPost', 'Admin\AdminController@statusPost');
    Route::get('statusTag', 'Admin\AdminController@statusTag');
    Route::get('statusPage', 'Admin\AdminController@statusPage');
    Route::get('statusNews', 'Admin\AdminController@statusNews');

	
});


Route::get('/admin-control', 'Admin\AdminController@login')->name('adminControl');
Route::post('/login', 'Admin\AuthController@login')->name('admin.login');
Route::get('/logout', 'Admin\AuthController@logout')->name('admin.logout');	

Route::get('/news/{news}', 'NewsController@show')->name('news.show');
Route::get('/news', 'NewsController@index')->name('news');

Route::get('/page/{page}', 'PagesController@show')->name('pages.show');

Route::any('search','FrontController@result')->name('search');

// Route::get('/page', 'FrontController@index1')->name('pages');

// Route::middleware(['auth', 'user'])->group(function(){
// 	Route::get('users/profile', 'UsersController@edit')->name('users.edit-profile');
// 	Route::put('users/profile', 'UsersController@update')->name('users.update-profile');
// 	Route::get('users', 'UsersController@index')->name('users.index');
// 	Route::post('users/{user}/make-admin', 'UsersController@makeAdmin')->name('users.make-admin');
// });

//Route::get('/admin-control', [AdminController::class, 'show'])->name('admin.show');
// Route::get('/admin-control', [AdminController::class, 'index'])->name('admin.index');

// Route::get('/papers',  'PapersController@index')->name('papers.index');
// Route::get('/papers/upload', 'PapersController@create')->name('papers.upload');
// Route::post('/papers/store',  'PapersController@store')->name('papers.store');
// Route::get('papers/{paper}/edit', 'PapersController@edit')->name('papers.edit');
// Route::delete('papers/{paper}',  'PapersController@destroy');

 // Route::post('profile/{profileId}/follow', 'ProfileController@followUser')->name('user.follow');
 // Route::post('/{profileId}/unfollow', 'ProfileController@unFollowUser')->name('user.unfollow');

Route::post('/ajax-image-upload','Admin\AdminController@Imagestore')->name('Imagestore');

