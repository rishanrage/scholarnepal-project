<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 
        'lname',
        'type', 
        'email', 
        'password',
        'location',
        'gender',
        'dob',
        'c_profession',
        'work_place',
        'a_degree',
        'a_institution',
        'r_areas',
        'name_institution', 
        'c_person',
        'phone',
        't_organisation',
        'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        return $this->role === 'admin';
    }

    public function isUser()
    {
        return $this->role === 'writer';
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function paper()
    {
        return $this->hasMany(Paper::class);
    }

      /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'leader_id', 'follower_id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followings()
    {
        return $this->belongsToMany(User::class, 'followers', 'follower_id', 'leader_id')->withTimestamps();
    }

// * Check if a given user is following this user.
    
    public function isFollowing(User $user)
    {
        return !! $this->followings()->where('leader_id', $user->id)->count();
    }


//Check if a given user is being followed by this user.

    public function isFollowedBy(User $user)
    {
        return !! $this->followers()->where('follower_id', $user->id)->count();
    }


    public function likes()
    {
      return $this->hasMany(Like::class);
    }

    


    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
    public function replies(){
        return $this->hasMany('App\CommentReply');
    }

}
