<?php

namespace App;

use App\Paper;
use Illuminate\Database\Eloquent\Model;


class Upload extends Model
{
    protected $fillable = ['paper_id','category_id', 'name'];

    public function paper()
    {
    	return $this->belongsTo(Paper::class);
    }
}