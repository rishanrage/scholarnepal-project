<?php

namespace App;

use App\Paper;
use Illuminate\Database\Eloquent\Model;


class Author extends Model
{
    protected $fillable = ['paper_id','fname', 'mname', 'lname'];

    public function paper()
    {
    	return $this->belongsTo(Paper::class);
    }
}