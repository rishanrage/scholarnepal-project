<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Posts\CreatePostsRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Posts\UpdatePostRequest;
use App\Post;
use App\Category;
use App\Tag;
use App\Page;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('VerifyCategoriesCount')->only(['create', 'store']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $user =  Auth::user();
       $posts = Post::where('user_id', $user->id)->get();  
       return view('posts.index', compact('user','posts'))
       ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('posts.create')
       ->with('categories', Category::all())->with('tags', Tag::all())
       ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostsRequest $request)
    {
       //  dd($request->all());
        // upload the image
       //  $image = $request->image->store('posts', 'uploads');

        if ($request->file('image') == null) {
            $image = 'images/no-image.png';
        }else{
           $image = $request->file('image')->store('posts', 'uploads');  
        }

        // create the post 

        $post = Post::create([
          'title' => $request->title,  
          'description' => $request->description,
          'content' => $request->content,
          'keywords' => $request->keywords,
          'image' => $image,
          'published_at' => $request->published_at,
          'category_id' => $request->category_id,
          'user_id' => auth()->user()->id         
        ]);

        if($request->tags) {
            $post->tags()->attach($request->tags);
        }

        // flash message
        session()->flash('success', 'Post created successfully.');

        // redirect
        return redirect(route('posts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.create')->with('post', $post)
        ->with('categories', Category::all())->with('tags', Tag::all())
        ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }

    /**
     * Update the specified resource in storage.->with('categories', Category::all())
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $data = $request->only(['title', 'description', 'published_at', 'content','keywords', 'category_id']);

        if($request->hasfile('image')) {
            $image = $request->file('image')->store('posts', 'uploads');
            $post->deleteImage();
            $data['image'] = $image;
        }

        if($request->tags) {
            $post->tags()->sync($request->tags);
        }

        $post->update($data);
        session()->flash('success', 'Post updated successfully.');
        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $post = Post::withTrashed()->where('id', $id)->firstOrFail();
 
        if($post->trashed()) {
            // deleting image from the folder too
            $post->deleteImage();
            $post->forceDelete();
        } else {
            $post->delete();
        }

        session()->flash('success', 'Post deleted successfully.');
        return redirect(route('posts.index'));
    }

    /**
     * Display the list of all trashed posts.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function trashed()
    {
        $trashed = Post::onlyTrashed()->get();

        return view('posts.index')->with('posts',$trashed)
        ->with('pages', Page::orderBy('ordering', 'asc')->get());

        // return view('posts.index')->withPosts($trashed); // 
        // withPosts($trashed) Or  with('posts',$trashed); Are same but the way of writing are different

        //$post = Post::withTrashed()->where('id', $id)->firstOrFail();

    }

    public function restore($id)
    {
       $post = Post::withTrashed()->where('id', $id)->firstOrFail(); 
       $post->restore();
       session()->flash('success', 'Post restored successfully.');
       return redirect()->back();
    }

}
