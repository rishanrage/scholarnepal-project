<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Tag;
use App\News;
use App\Page;
use Illuminate\Support\Facades\DB;

class FrontController extends Controller
{
    public function index(Category $category)
    {
    	return view('front.index')
    	->with('categories', Category::inRandomOrder()->get()->take(6))
    	->with('tags', Tag::all()->take(20))
    	->with('news', News::all())
    	->with('pages', Page::orderBy('ordering', 'asc')->get())
    	->with('posts', Post::orderBy('id', 'desc')->get())
    	->with('featured', Category::where('featured', 1)->get()->take(3));
    }
    
    public function result(Request  $request)
    {
        $result=Post::where('title', 'LIKE', "%{$request->input('query')}%")
                 ->orWhere('description', 'LIKE', "%{$request->input('query')}%")
                 ->orWhere('content', 'LIKE', "%{$request->input('query')}%")
                 ->orWhere('keywords', 'LIKE', "%{$request->input('query')}%")
                 ->get();
                 
                 
        
        // dd($result);
        // return response()->json($result);
        return view('search', compact('result')) 
               ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }
    
    
}
