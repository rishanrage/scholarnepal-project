<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Paper;
use App\Upload;
use App\Category;
use App\Page;
use App\Author;

use App\Http\Requests\Uploads\CreateUploadRequest;

class PapersController extends Controller
{

	public function index()
    {

      $user =  Auth::user();
      $papers = Paper::where('user_id', $user->id)->get();  
      return view('papers.index', compact('user','papers'))
      ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }

    public function create()
    {
    	return view('papers.create')
      ->with('categories', Category::all())
      ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }


    public function store(CreateUploadRequest $request)
    {

    	$user  = Auth::user();
    	$paper = new Paper();
    	$data  = $request->all();
      $cat = $request->category_id;
     //dd($cat);
      $paper = $user->paper()->create($data);


    	if($request->hasFile('upload')) {
              $files = $request->file('upload');

              foreach($files as $file) {
              	$name = time().'-'.$file->getClientOriginalName();
              	$name = str_replace(' ', '-', $name);
           //   	echo $name."<br/>";
              	$file->move('data/papers', $name);
                $paper->upload()->create(['name'=>$name, 'category_id'=>$cat]);
              }

    	}



        foreach($request->fname as $key=>$fname){
               $data = new Author();
               $fname = $fname;
               $mname = $request->mname[$key];
               $lname = $request->lname[$key];
               // $data->save();
               $paper->author()->create(['fname'=>$fname, 'mname'=>$mname, 'lname'=>$lname]);
            }



        session()->flash('success', 'Paper uploaded successfully.'); 
      	return redirect(route('papers.index'));

    }


    public function edit(Paper $paper)
    {   
       $uploads   = Upload::where('paper_id', $paper->id)->get();        
       return view('papers.edit')
       ->with('paper', $paper)->with('uploads', $uploads)
       ->with('categories', Category::all())
       ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }

    public function destroy(Paper $paper)
    {
        $paper->delete();
        session()->flash('success', 'Paper deleted successfully.');
        return redirect(route('papers.index'));
    }
}
