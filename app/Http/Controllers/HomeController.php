<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Page;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index()
    // {
    //     $user = User::where('user_id', $user->id)->get();
    //     return view('home')->with('user', '$user');
    // }

    //     public function edit(Tag $Tag)
    // {
    //     return view('tags.create')->with('tag', $Tag);
    // }


    public function index()
    {
        $user = auth()->user();

      //  dd($user);

        //$user = User::where('user_id', $user->id)->get();
        $followers  = $user->followers;
        $followings = $user->followings;  

        // dd($followers);  

        return view('home', compact('user', 'followers', 'followings'))
        ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }

}



