<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\News\CreateNewsRequest;
use App\Http\Requests\News\UpdateNewsRequest;
use App\News;
use App\Page;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
	public function index()
    {     
        return view('admin.news.index')->with('news', News::all());
    }

    public function create()
    {
        return view('admin.news.create');
    }


    public function store(CreateNewsRequest $request)
    {
       //  dd($request->all());
        // upload the image
       //  $image = $request->image->store('posts', 'uploads');

        if ($request->file('image') == null) {
            $image = 'images/no-image.png';
        }else{
           $image = $request->file('image')->store('pages', 'uploads');  
        }

        // create the post 

        $news = News::create([
          'title' => $request->title,  
          'description' => $request->description,
          'content' => $request->content,
          'image' => $image  
        ]);

        // flash message
        session()->flash('success', 'News created successfully.');

        // redirect
        return redirect(route('admin.news'));
    }


    public function edit(News $news)
    {
        return view('admin.news.create')->with('news', $news);
    }


    public function update(UpdateNewsRequest $request, News $news)
    {
        $data = $request->only(['title', 'description', 'content']);

        if($request->hasfile('image')) {
            $image = $request->file('image')->store('pages', 'uploads');
            $news->deleteImage();
            $data['image'] = $image;
        }


        $news->update($data);
        session()->flash('success', 'News updated successfully.');
        return redirect(route('admin.news'));
    }


    public function destroy(News $news)
    {
        
        $news->delete();
        session()->flash('success', 'News deleted successfully.');
        return redirect(route('admin.news'));
    }

    public function show(News $news)
    {
        DB::table("news")->where('id', $news->id);
        return view('news.show')->with('news', $news)
        ->with('releated', News::all())
        ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }


}
