<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Post;
use App\Tag;
use App\Category;
use App\Like;
use App\Paper;
use App\Upload;
use App\Page;
use Illuminate\Support\Facades\DB;



class PostsController extends Controller
{
    public function show(Post $post)
    {
        DB::table("posts")->where('id', $post->id)->increment('views');
    	return view('blog.show')->with('post', $post)
        ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }

    public function category(Category $category)
    {
    	// $list = Post::where('category_id', $post->id)->orderBy('published_at','desc')->get();
    	// $category   = Category::where('id', $post->id)->first();
    	// return view('blog.list')->with('list', $list)->with('category', $category);

         // return view('blog.list')->with('category', $category)->with('posts', $category->posts()->simplePaginate(1)); 
        return view('blog.list')->with('category', $category)
                                ->with('posts', Post::where('category_id', $category->id)->orderBy('id', 'desc')->get())
                                ->with('pages', Page::orderBy('ordering', 'asc')->get()); 
    }

    public function writer(Post $post)
    {

    	$writerPost = Post::where('user_id', $post->id)->orderBy('published_at','desc')->get();
    	$writerProfile   = User::where('id', $post->id)->first();
        $followers  = $writerProfile->followers;
        $papers = Paper::where('user_id', $writerProfile)->get();  
    	return view('blog.writer')->with('writerPost', $writerPost)
                                  ->with('writerProfile', $writerProfile)
                                  ->with('followers', $followers)
                                  ->with('papers', $papers)
                                  ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }


    public function paper(Post $post, Paper $paper)
    {

        $writerProfile   = User::where('id', $post->id)->first(); 
        $followers  = $writerProfile->followers;     
        $papers = Paper::where('user_id', $writerProfile->id)->get(); 

        return view('blog.paper')->with('papers', $papers)
                                 ->with('followers', $followers)
                                 ->with('writerProfile', $writerProfile )
                                 ->with('pages', Page::orderBy('ordering', 'asc')->get());

    }

    public function paperByCid(Category $category)
    {
      
        $papers = Paper::where('category_id', $category->id)->get(); 
        $cat   = Category::where('id', $category->id)->first(); 
        return view('blog.papercategory')
        ->with('papers', $papers)
        ->with('cat', $cat)
        ->with('pages', Page::orderBy('ordering', 'asc')->get());

    }

    public function uploads(Paper $paper)
    {
        $uploads = Upload::where('paper_id', $paper->id)->get();    
       // dd($uploads);
        return view('blog.uploads')->with('uploads', $uploads)
        ->with('paper', $paper)
        ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }


    public function tag(Tag $tag) {
        return view('blog.tag')->with('tag', $tag)
                               ->with('posts', $tag->posts, Post::orderBy('id', 'desc')->get())
                               ->with('pages', Page::orderBy('ordering', 'asc')->get()); 
    }


    public function LikePost(Request $request)
    {
        $post_id = $request['postId'];
        $is_like = $request['isLike'] === 'true';
        $update = false;
        $post = Post::find($post_id);
        if (!$post) {
            return null;
        }
        $user = Auth::user();
        $like = $user->likes()->where('post_id', $post_id)->first();
        if ($like) {
            $already_like = $like->like;
            $update = true;
            if ($already_like == $is_like) {
                $like->delete();
                return null;
            }
        } else {
            $like = new Like();
        }
        $like->like = $is_like;
        $like->user_id = $user->id;
        $like->post_id = $post->id;
        if ($update) {
            $like->update();
        } else {
            $like->save();
        }
        return null;
    }


}
