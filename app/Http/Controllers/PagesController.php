<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Pages\CreatePagesRequest;
use App\Http\Requests\Pages\UpdatePagesRequest;
use App\Page;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
	public function index()
    {     
        return view('admin.menu.index')->with('pages', Page::all());
    }

    public function create()
    {
        return view('admin.menu.create');
    }


    public function store(CreatePagesRequest $request)
    {
       //  dd($request->all());
        // upload the image
       //  $image = $request->image->store('posts', 'uploads');

        if ($request->file('image') == null) {
            $image = 'images/no-image.png';
        }else{
           $image = $request->file('image')->store('pages', 'uploads');  
        }

        // create the post 

        $post = Page::create([
          'title' => $request->title,  
          'description' => $request->description,
          'content' => $request->content,
          'ordering' => $request->ordering,
          'image' => $image  
        ]);

        // flash message
        session()->flash('success', 'Page created successfully.');

        // redirect
        return redirect(route('admin.pages'));
    }


    public function edit(Page $page)
    {
        return view('admin.menu.create')->with('page', $page);
    }


    public function update(UpdatePagesRequest $request, Page $page)
    {
        $data = $request->only(['title', 'description', 'content', 'ordering']);

        if($request->hasfile('image')) {
            $image = $request->file('image')->store('pages', 'uploads');
            $page->deleteImage();
            $data['image'] = $image;
        }


        $page->update($data);
        session()->flash('success', 'Page updated successfully.');
        return redirect(route('admin.pages'));
    }


    public function destroy(Page $page)
    {
        
        $page->delete();
        session()->flash('success', 'Page deleted successfully.');
        return redirect(route('admin.pages'));
    }

    public function show(Page $page)
    {
        DB::table("pages")->where('id', $page->id);
        return view('pages.show')
        ->with('page', $page)
        ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }



}
