<?php

namespace App\Http\Controllers;

use App\User;
use App\Page;
use Illuminate\Http\Request;

use App\Http\Requests\Users\UpdateProfileRequest;

class UsersController extends Controller
{
    public function index()
    {
    	return view('users.index')
        ->with('users', User::all())
        ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }

    public function edit()
    {
        return view('users.edit')
        ->with('user', auth()->user())
        ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }

    public function update(UpdateProfileRequest $request)
    {

         $data = $request->only([
            'fname'             => $request->fname,
            'lname'             => $request->lname,
            'location'          => $request->location,
            'gender'            => $request->gender,
            'dob'               => $request->dob,
            'c_profession'      => $request->c_profession,
            'work_place'        => $request->work_place,
            'a_degree'          => $request->a_degree,
            'a_institution'     => $request->a_institution,
            'r_areas'           => $request->r_areas,
            'name_institution'  => $request->name_institution,
            'c_person'          => $request->c_person,
            'phone'             => $request->phone,
            't_organisation'    => $request->t_organisation,
        ]);

        if($request->hasfile('image')) {
            $image = $request->file('image')->store('users', 'uploads');
            $data['image'] = $image;
        }


     // dd($request);
        $user = auth()->user();
        $user->update($data);

        session()->flash('success', 'Profile updated successfully');
        return redirect()->back();

    }

    public function makeAdmin(User $user)
    {
    	$user->role = 'admin';
    	$user->save();
    	session()->flash('success', 'User make admin successfully.');
    	return redirect(route('users.index'));
    }
}
