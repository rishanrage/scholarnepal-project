<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Page;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Tags\CreateTagRequest;
use App\Http\Requests\Tags\UpdateTagsRequest;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        $user =  Auth::user();
        return view('tags.index')
        ->with('tags', Tag::where('user_id', $user->id)->get())
        ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }
    
    // $user =  Auth::user();
    //   $posts = Post::where('user_id', $user->id)->get();

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tags.create')
        ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTagRequest $request)
    {
        
        Tag::create([
            'name' => $request->name,
            'user_id' => auth()->user()->id
        ]);
        session()->flash('success', 'Tag created successfully.'); 
        return redirect(route('tags.index'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $Tag)
    {
        return view('tags.create')
        ->with('tag', $Tag)
        ->with('pages', Page::orderBy('ordering', 'asc')->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTagsRequest $request, Tag $tag)
    {
        $tag->update([
           'name' => $request->name,
           'user_id' => auth()->user()->id
        ]);

        $tag->save();
        session()->flash('success', 'Tag updated successfully.');
        return redirect(route('tags.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        if($tag->posts->count() > 0) {
               session()->flash('error', 'Tag cannot be deleted because it is associated to some posts.');
               return redirect()->back();
        }
        $tag->delete();
        session()->flash('success', 'Tag deleted successfully.');
        return redirect(route('tags.index'));
    }
}
