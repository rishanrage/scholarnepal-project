<?php

namespace App\Http\Controllers\Admin;
use App\User;
use App\Post;
use App\Category;
use App\Tag;
use App\Page;
use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;


class AdminController extends Controller
{
    public function login()
    {
    	return view('admin.login');
    }

    public function dashboard()
    {
    	return view('admin.index');
    }

    public function users()
    {
    	return view('admin.pages.users')->with('users', User::all());
    }

    public function posts()
    {
    	return view('admin.pages.posts')->with('posts', Post::all());
    }

    public function categories()
    {
    	return view('admin.pages.categories')->with('categories', Category::all());
    }

    public function tags()
    {
    	return view('admin.pages.tags')->with('tags', Tag::all());
    }

    public function pages()
    {
        return view('admin.menu.index');
    }

    public function create()
    {
        return view('admin.menu.create');
    }

    public function featuredCategories(Request $request)
    {
        $id = Category::find($request->id);
        $id->featured = $request->featured;
        $id->save();

        return response()->json(['success'=>'Featured change successfully.']);
    }

    public function statusCategories(Request $request)
    {
        $id = Category::find($request->id);
        $id->status = $request->status;
        $id->save();

        return response()->json(['success'=>'Category Status change successfully.']);
    }

    public function statusUsers(Request $request)
    {
        $id = User::find($request->id);
        $id->status = $request->status;
        $id->save();

        return response()->json(['success'=>'User status change successfully.']);
    }

    public function statusPost(Request $request)
    {
        $id = Post::find($request->id);
        $id->status = $request->status;
        $id->save();

        return response()->json(['success'=>'Post status change successfully.']);
    }

    public function statusTag(Request $request)
    {
        $id = Tag::find($request->id);
        $id->status = $request->status;
        $id->save();

        return response()->json(['success'=>'Tag status change successfully.']);
    }

    public function statusPage(Request $request)
    {
        $id = Page::find($request->id);
        $id->status = $request->status;
        $id->save();

        return response()->json(['success'=>'Page status change successfully.']);
    }

    public function statusNews(Request $request)
    {
        $id = News::find($request->id);
        $id->status = $request->status;
        $id->save();

        return response()->json(['success'=>'News status change successfully.']);
    }
    
    
    public function Imagestore(Request $request)
    {
        
         $imageUpdate = Category::find($request->id);
  
          if($request->hasFile('icon')) {
              
                 $imageName = time().'.'.request()->icon->getClientOriginalExtension();
                 request()->icon->move(public_path('categories'), $imageName);
                 
                 $imageUpdate->update(['icon', $imageName]);
                 
          } 

        return Response()->json(["success"=>"Image Upload Successfully"]);
            
         


    }

}
