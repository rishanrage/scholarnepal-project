<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class News extends Model
{
   protected $fillable = [
      'title', 'description', 'content', 'image'
    ];


    public function deleteImage()
    {
      File::delete(public_path('data/'.$this->image));
    }
}
