<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File; 

class Post extends Model
{
	use SoftDeletes;
	
    protected $fillable = [
      'title', 'description', 'content', 'image', 'keywords', 'published_at', 'category_id', 'user_id'
    ];

    /**
     * Delete image from the folder
     *
     * @return void
     */

    public function deleteImage()
    {
      File::delete(public_path('data/'.$this->image));
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function hasTag($tagId)
    {
        return in_array($tagId, $this->tags->pluck('id')->toArray());
    }

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function likes()
    {
      return $this->hasMany(Like::class);
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }


}
