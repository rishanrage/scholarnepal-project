<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Page extends Model
{
   protected $fillable = [
      'title', 'description', 'content', 'image', 'ordering'
    ];


    public function deleteImage()
    {
      File::delete(public_path('data/'.$this->image));
    }
}
