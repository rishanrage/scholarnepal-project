<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Category;
use App\Author;


class Paper extends Model
{
    protected $fillable = ['category_id', 'title','body', 'published_in', 'doi', 'keywords', 'published_at'];


    public function user()
    {
    	return $this->belongsTo(User::class);
    }


    public function upload()
    {
    	return $this->hasMany(Upload::class);
    }

    public function author()
    {
    	return $this->hasMany(Author::class);
    }


    

}
